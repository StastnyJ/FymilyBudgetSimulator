using System;
using DatabaseModels;
using DatabaseRepositories;
using System.Linq;
using System.Collections.Generic;
using Utils;

namespace FamilyBudgetSimulator.Jobs
{
    public class ScheduleJobManager
    {
        private readonly FBSDbContext context;
        private readonly ActionRepository actionRep;
        private readonly BankingRepository bankingRep;
        private readonly UserRepository userRep;
        private static bool locked = false;

        public ScheduleJobManager(FBSDbContext context)
        {
            this.context = context;
            actionRep = new ActionRepository(context);
            bankingRep = new BankingRepository(context);
            userRep = new UserRepository(context);
        }

        public void MenageScheduleJob()
        {
            if(locked) return;
            locked = true;
            try
            {
                var waitigActions = actionRep.getWaitingActions();
                foreach(ScheduleAction action in waitigActions)
                {
                    if(ManageWaitingAction(action))
                    {
                        actionRep.DeleteAction(action, false);
                    }
                }
                context.SaveChanges();
            }
            finally
            {
                locked = false;
            }
        }

        public bool ManageWaitingAction(ScheduleAction action)
        {
            try
            {
                switch(action.ActionTypeId)
                {
                    case ScheduleActionType.PaySavingsAccountAction:
                        return PayAccount(new PaySavingsAccountAction(action));

                    case ScheduleActionType.HouseholdExpenditureAction:
                        return PayHouseHoldExpenditure(new HouseholdExpenditureAction(action));
                    
                    case ScheduleActionType.InvestitionFondInterest:
                        return FondInterest(new InvestitionFondInterestAction(action));
                    
                    case ScheduleActionType.PayInsuranceAction:
                        return PayInsurance(new PayInsuranceAction(action));
                }
            }
            catch
            {
                return false;
            }
            return false;
        }

        private bool PayAccount(PaySavingsAccountAction action)
        {
            bool res = bankingRep.PaySavingsAccount(action.User.Properties.Account, bankingRep.GetSavingsAccount(action.User.Properties.AccountNumber, action.TimeOfSavingsAccountCreation), !action.CollectMoney);            
            if(action.CollectMoney && res)
            {
                SavingsAccount account = bankingRep.GetSavingsAccount(action.User.Properties.AccountNumber, action.TimeOfSavingsAccountCreation);
                bankingRep.UpdateBalanceAndSaveHistory(action.User.Properties.Account, account.MoneyWithInterest + account.Subsidy, ApplicationConstants.SYSTEM_ACCOUNT_NUMBER, 1, "Money from your savings account", false);
                res = bankingRep.RemoveSavingsAccount(account, true);
            }
            return res;
        }

        private bool PayHouseHoldExpenditure(HouseholdExpenditureAction action)
        {
            if(action.User.Properties.Account.Balance < action.User.Properties.HouseholdExpenditure) return false;
            bankingRep.UpdateBalanceAndSaveHistory(action.User.Properties.Account, -1 * action.User.Properties.HouseholdExpenditure, ApplicationConstants.SYSTEM_ACCOUNT_NUMBER, 1, "Household expenditure payment", false);   
            return actionRep.CreatePayHouseholdHexpeditureAction(action.User);
        }
        
        private bool FondInterest(InvestitionFondInterestAction action)
        {
            Fond fond = bankingRep.GetFond(action.User.Properties.Account, action.TimeOfFondCreation);
            bankingRep.InterestFond(fond, false);
            return actionRep.CreateInvestitionFondInterestAction(fond, action.User);
        }

        private bool PayInsurance(PayInsuranceAction action)
        {
            Insurance insurance = action.Insurance;
            if(action.User.Properties.Account.Balance < insurance.MonthlyFee) return false;
            bankingRep.UpdateBalanceAndSaveHistory(action.User.Properties.Account, -insurance.MonthlyFee, ApplicationConstants.SYSTEM_ACCOUNT_NUMBER, 0, "Schedule payment for " + insurance.ToString(), false);
            return actionRep.CreateInsuranceAction(action.User, insurance);
        }
    }
}