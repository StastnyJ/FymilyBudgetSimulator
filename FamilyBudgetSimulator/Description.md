Učitel
===============
nastavení
-----------
- zvolí žákům povolání
- rozdělí žáky do dvojic
- nastavení čas virtuálního měsíce

Průběh
----------
- sledování průběhu akce
- finanční podpora

Žák
===============

Práce
----------
- dostává výplatu
- může dostat výpověď na 4 měsíce
- Podrobnosti <a href="https://docs.google.com/spreadsheets/d/1LQm-BZ9ImuB6kEbEReziup6w293EXZ0VJwWPCZ5gZZY/edit?usp=sharing"> zde </a>

Majetek
---------
- získávní (asi automatické) majetku a případně jeho správa

Rodina
--------
- automaticky odebírány peníze na **běžný rodinný provoz**
- možnost rozhodování o větších výdajích jako je dovolená a pod
- plánování dětí

Bankovnictví
---------
- vesměs zjednodušená kopie internet bankingu

Pojišťovny
--------
- možnost určitého finančního zajištění

Život
-------
- index šťestí
- náhodné generování určitých životních událostí

[https://docs.elmah.io/logging-to-elmah-io-from-aspnet-core/]