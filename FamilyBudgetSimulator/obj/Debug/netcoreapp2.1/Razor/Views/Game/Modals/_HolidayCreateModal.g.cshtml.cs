#pragma checksum "/home/stastnyj/FBS/FymilyBudgetSimulator/FamilyBudgetSimulator/Views/Game/Modals/_HolidayCreateModal.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "278019ad0b30b170be9f3eea63943ba06c49fa1e"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Game_Modals__HolidayCreateModal), @"mvc.1.0.view", @"/Views/Game/Modals/_HolidayCreateModal.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Game/Modals/_HolidayCreateModal.cshtml", typeof(AspNetCore.Views_Game_Modals__HolidayCreateModal))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "/home/stastnyj/FBS/FymilyBudgetSimulator/FamilyBudgetSimulator/Views/_ViewImports.cshtml"
using FamilyBudgetSimulator;

#line default
#line hidden
#line 2 "/home/stastnyj/FBS/FymilyBudgetSimulator/FamilyBudgetSimulator/Views/_ViewImports.cshtml"
using FamilyBudgetSimulator.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"278019ad0b30b170be9f3eea63943ba06c49fa1e", @"/Views/Game/Modals/_HolidayCreateModal.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3ebcf499ba6f0f7a0c8596c7b1d20ee2fa1f9dd1", @"/Views/_ViewImports.cshtml")]
    public class Views_Game_Modals__HolidayCreateModal : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 1491, true);
            WriteLiteral(@"<div class=""modal fade"" id=""HolidayCreateModal"" tabindex=""-1"" role=""dialog"" aria-labelledby=""HolidayCreateModalLabel"" aria-hidden=""true"">
    <div class=""modal-dialog modal-lg"" role=""document"">
        <div class=""modal-content"">
            <div class=""modal-header"">
                <h5 class=""modal-title"" id=""HolidayCreateModalLabel"">Book holiday</h5>
            </div>
            <div class=""modal-body"">
                <div class=""row"">
                    <div class=""col-md-2"">
                        <label>From </label>
                    </div>
                    <div class=""col-md-4"">
                        <input type=""text"" id=""dateFrom"" value=""20-08-2016"" class=""form-control"" onkeydown=""return false"" data-toggle=""datetimepicker"" data-target=""#dateFrom""/>
                    </div>
                    <div class=""col-md-2"">
                        <label>To </label>
                    </div>
                    <div class=""col-md-4"">
                        <input type=""text"" id=""dateTo"" value");
            WriteLiteral(@"=""30-08-2016""  class=""form-control"" onkeydown=""return false"" data-toggle=""datetimepicker"" data-target=""#dateTo""/>
                    </div>
                </div>
                <div class=""row"">
                    <div class=""col-md-2"">
                        <label>Travel agency </label>
                    </div>
                    <div class=""col-md-4"">
                        <select class=""form-control"" style=""width: 100%"">
                            ");
            EndContext();
            BeginContext(1491, 35, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "4c0105cce2f347089e0b9348eeb03b25", async() => {
                BeginContext(1514, 3, true);
                WriteLiteral("All");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1526, 29, true);
            WriteLiteral("\n                            ");
            EndContext();
            BeginContext(1555, 21, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "48caea1a15cd46349f20d72b771eb466", async() => {
                BeginContext(1563, 4, true);
                WriteLiteral("CK 1");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1576, 29, true);
            WriteLiteral("\n                            ");
            EndContext();
            BeginContext(1605, 21, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "53a1878dac044c7d86dc7aa6130546c0", async() => {
                BeginContext(1613, 4, true);
                WriteLiteral("CK 2");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1626, 29, true);
            WriteLiteral("\n                            ");
            EndContext();
            BeginContext(1655, 21, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d3386cdd18664f8cb71c74d6da4d3853", async() => {
                BeginContext(1663, 4, true);
                WriteLiteral("CK 3");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1676, 29, true);
            WriteLiteral("\n                            ");
            EndContext();
            BeginContext(1705, 21, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0385638ff2ae4fbb87816d348e7a353b", async() => {
                BeginContext(1713, 4, true);
                WriteLiteral("CK 4");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1726, 29, true);
            WriteLiteral("\n                            ");
            EndContext();
            BeginContext(1755, 21, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7674d95139fa45d0867b8d18b122cc2f", async() => {
                BeginContext(1763, 4, true);
                WriteLiteral("CK 5");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1776, 329, true);
            WriteLiteral(@"
                        </select>
                    </div>
                    <div class=""col-md-2"">
                        <label>Destination </label>
                    </div>
                    <div class=""col-md-4"">
                        <select class=""form-control"" style=""width: 100%"">
                            ");
            EndContext();
            BeginContext(2105, 35, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "acbee37894c942c383aed7df58229309", async() => {
                BeginContext(2128, 3, true);
                WriteLiteral("All");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2140, 29, true);
            WriteLiteral("\n                            ");
            EndContext();
            BeginContext(2169, 23, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "497beb910fce4eec9b10f6749497c0cc", async() => {
                BeginContext(2177, 6, true);
                WriteLiteral("Dest 1");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2192, 29, true);
            WriteLiteral("\n                            ");
            EndContext();
            BeginContext(2221, 23, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "11925fa4ce934daeb24a1693c84a0a40", async() => {
                BeginContext(2229, 6, true);
                WriteLiteral("Dest 2");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2244, 29, true);
            WriteLiteral("\n                            ");
            EndContext();
            BeginContext(2273, 23, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "4c0a7d104fde419abe5bdc1e2614b9f5", async() => {
                BeginContext(2281, 6, true);
                WriteLiteral("Dest 3");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2296, 29, true);
            WriteLiteral("\n                            ");
            EndContext();
            BeginContext(2325, 23, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "c417ba4e2c0d40cbbd332e710fbbed1a", async() => {
                BeginContext(2333, 6, true);
                WriteLiteral("Dest 4");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2348, 29, true);
            WriteLiteral("\n                            ");
            EndContext();
            BeginContext(2377, 23, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "ad15d6d237d54065871745bd29f47f51", async() => {
                BeginContext(2385, 6, true);
                WriteLiteral("Dest 5");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2400, 7990, true);
            WriteLiteral(@"                        
                        </select>
                    </div>                    
                </div>
                <hr>
                <table style=""width: 100%"" class=""table table-hover"" id=""BookHolidayTable"">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Happiness</th>
                            <th>Holidays cost</th>
                            <th>Destination</th>
                            <th>Travel agency</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class=""selectable"">
                            <td>Holiday in Greece</td>
                            <td>€ 500</td>
                            <td>172</td>
                            <td>7</td>  
                            <td>Greece</td>      
                            <td>Fisher</td>
                        </tr>
   ");
            WriteLiteral(@"                     <tr class=""selectable"">
                            <td>Holiday in Greece</td>
                            <td>€ 500</td>
                            <td>172</td>
                            <td>7</td>  
                            <td>Greece</td>      
                            <td>Fisher</td>
                        </tr>
                        <tr class=""selectable"">
                            <td>Holiday in Greece</td>
                            <td>€ 500</td>
                            <td>172</td>
                            <td>7</td>  
                            <td>Greece</td>      
                            <td>Fisher</td>
                        </tr>
                        <tr class=""selectable"">
                            <td>Holiday in Greece</td>
                            <td>€ 500</td>
                            <td>172</td>
                            <td>7</td>  
                            <td>Greece</td>      
                            <td>Fisher</td>
 ");
            WriteLiteral(@"                       </tr>
                        <tr class=""selectable"">
                            <td>Holiday in Greece</td>
                            <td>€ 500</td>
                            <td>172</td>
                            <td>7</td>  
                            <td>Greece</td>      
                            <td>Fisher</td>
                        </tr>
                        <tr class=""selectable"">
                            <td>Holiday in Greece</td>
                            <td>€ 500</td>
                            <td>172</td>
                            <td>7</td>  
                            <td>Greece</td>      
                            <td>Fisher</td>
                        </tr>
                        <tr class=""selectable"">
                            <td>Holiday in Greece</td>
                            <td>€ 500</td>
                            <td>172</td>
                            <td>7</td>  
                            <td>Greece</td>      
             ");
            WriteLiteral(@"               <td>Fisher</td>
                        </tr>
                        <tr class=""selectable"">
                            <td>Holiday in Greece</td>
                            <td>€ 500</td>
                            <td>172</td>
                            <td>7</td>  
                            <td>Greece</td>      
                            <td>Fisher</td>
                        </tr>
                        <tr class=""selectable"">
                            <td>Holiday in Greece</td>
                            <td>€ 500</td>
                            <td>172</td>
                            <td>7</td>  
                            <td>Greece</td>      
                            <td>Fisher</td>
                        </tr>
                        <tr class=""selectable"">
                            <td>Holiday in Greece</td>
                            <td>€ 500</td>
                            <td>172</td>
                            <td>7</td>  
                            <td");
            WriteLiteral(@">Greece</td>      
                            <td>Fisher</td>
                        </tr>
                        <tr class=""selectable"">
                            <td>Holiday in Greece</td>
                            <td>€ 500</td>
                            <td>172</td>
                            <td>7</td>  
                            <td>Greece</td>      
                            <td>Fisher</td>
                        </tr>
                        <tr class=""selectable"">
                            <td>Holiday in Greece</td>
                            <td>€ 500</td>
                            <td>172</td>
                            <td>7</td>  
                            <td>Greece</td>      
                            <td>Fisher</td>
                        </tr>
                        <tr class=""selectable"">
                            <td>Holiday in Greece</td>
                            <td>€ 500</td>
                            <td>172</td>
                            <td>7</td>  ");
            WriteLiteral(@"
                            <td>Greece</td>      
                            <td>Fisher</td>
                        </tr>
                        <tr class=""selectable"">
                            <td>Holiday in Greece</td>
                            <td>€ 500</td>
                            <td>172</td>
                            <td>7</td>  
                            <td>Greece</td>      
                            <td>Fisher</td>
                        </tr>
                        <tr class=""selectable"">
                            <td>Holiday in Greece</td>
                            <td>€ 500</td>
                            <td>172</td>
                            <td>7</td>  
                            <td>Greece</td>      
                            <td>Fisher</td>
                        </tr>
                        <tr class=""selectable"">
                            <td>Holiday in Greece</td>
                            <td>€ 500</td>
                            <td>172</td>
        ");
            WriteLiteral(@"                    <td>7</td>  
                            <td>Greece</td>      
                            <td>Fisher</td>
                        </tr>
                        <tr class=""selectable"">
                            <td>Holiday in Greece</td>
                            <td>€ 500</td>
                            <td>172</td>
                            <td>7</td>  
                            <td>Greece</td>      
                            <td>Fisher</td>
                        </tr>
                        <tr class=""selectable"">
                            <td>Holiday in Greece</td>
                            <td>€ 500</td>
                            <td>172</td>
                            <td>7</td>  
                            <td>Greece</td>      
                            <td>Fisher</td>
                        </tr>
                        <tr class=""selectable"">
                            <td>Holiday in Greece</td>
                            <td>€ 500</td>
                 ");
            WriteLiteral(@"           <td>172</td>
                            <td>7</td>  
                            <td>Greece</td>      
                            <td>Fisher</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class=""modal-footer"">
            <div style=""width: 100%"">
                <input type=""checkbox"" id=""toggleone"" data-width=""120""  data-toggle=""toggle"" data-on=""Alone"" data-off=""With family"" data-onstyle=""success"" data-offstyle=""warning"">
                <button type=""button"" style=""float: right; margin-left: 10px"" class=""btn btn-primary"">Yes</button>
                <button type=""button"" style=""float: right"" class=""btn btn-secondary"" data-dismiss=""modal"">No</button>
            </div>
            </div>
        </div>
    </div>
</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
