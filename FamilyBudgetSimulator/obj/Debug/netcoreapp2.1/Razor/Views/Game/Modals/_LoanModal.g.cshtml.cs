#pragma checksum "/home/stastnyj/FBS/FymilyBudgetSimulator/FamilyBudgetSimulator/Views/Game/Modals/_LoanModal.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a8fe0f2fb3f471c0e2406becbb05f0eda365f6a2"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Game_Modals__LoanModal), @"mvc.1.0.view", @"/Views/Game/Modals/_LoanModal.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Game/Modals/_LoanModal.cshtml", typeof(AspNetCore.Views_Game_Modals__LoanModal))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "/home/stastnyj/FBS/FymilyBudgetSimulator/FamilyBudgetSimulator/Views/_ViewImports.cshtml"
using FamilyBudgetSimulator;

#line default
#line hidden
#line 2 "/home/stastnyj/FBS/FymilyBudgetSimulator/FamilyBudgetSimulator/Views/_ViewImports.cshtml"
using FamilyBudgetSimulator.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a8fe0f2fb3f471c0e2406becbb05f0eda365f6a2", @"/Views/Game/Modals/_LoanModal.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3ebcf499ba6f0f7a0c8596c7b1d20ee2fa1f9dd1", @"/Views/_ViewImports.cshtml")]
    public class Views_Game_Modals__LoanModal : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 1728, true);
            WriteLiteral(@"<div class=""modal fade"" id=""LoanModal"" tabindex=""-1"" role=""dialog"" aria-labelledby=""LoanModalLabel"" aria-hidden=""true"">
    <div class=""modal-dialog"" role=""document"">
        <div class=""modal-content"">
            <div class=""modal-header"">
                <h5 class=""modal-title"" id=""LoanModalLabel"">Ask for Loan</h5>
            </div>
            <div class=""modal-body"">
                <table>
                    <tr>
                        <td>
                            <label>
                                Ammount
                            </label>
                        </td>
                        <td style=""padding: 10px"">
                            <input type=""number"" class=""form-control"" placeholder=""Ammount"" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>
                                Monthly installment
                            </label>
                        </td>
                ");
            WriteLiteral(@"        <td style=""padding: 10px"">
                            <input type=""number"" class=""form-control"" placeholder=""Monthly installment"" />
                        </td>
                    </tr>
                </table>
                <br/>
                <label>Interest: 10 % (€ 800)</label><br/>
                <label>Total installment: € 8 800</label><br/>
                <label>Number of installments: 11</label>
            </div>
            <div class=""modal-footer"">
                <button type=""button"" class=""btn btn-secondary"" data-dismiss=""modal"">Cancel</button>
                <button type=""button"" class=""btn btn-primary"">Ask</button>
    		</div>
        </div>
    </div>
</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
