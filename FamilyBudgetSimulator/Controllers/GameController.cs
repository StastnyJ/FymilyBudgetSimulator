using System;
using System.Globalization;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FamilyBudgetSimulator.Models;
using DatabaseModels;
using DatabaseRepositories;
using FamilyBudgetSimulator.Attributes;
using Utils;

namespace FamilyBudgetSimulator.Controllers
{
    public class GameController : BaseController
    {
        private readonly FBSDbContext FBSS;
        private readonly UserRepository userRep;
        private readonly BankingRepository bankRep;
        private readonly ActionRepository actionRep;

        public GameController(FBSDbContext context)
        {
            FBSS = context;    
            userRep = new UserRepository(FBSS);
            bankRep = new BankingRepository(FBSS);
            actionRep = new ActionRepository(FBSS); 
        }

        //GET: "server"/Student/Life
        [VerifyUser]
        public ActionResult Index()
        {   
            ScheduleAction a = FBSS.WaitingActions.First();
            PaySavingsAccountAction aa = new PaySavingsAccountAction(a);
            aa.CollectMoney = true;
            ScheduleAction b = (ScheduleAction)aa;
            User usr = getLoggedUser(userRep);
            return View();
        }

        //GET: "server"/Game/Banking
        [VerifyUser]
        public ActionResult Banking()
        {
            User usr = getLoggedUser(userRep);
            BankingModel model = new BankingModel(){
                AccountNumber = usr.Properties.AccountNumber,
                Balance = usr.Properties.Account.Balance,
                NearestHistory = bankRep.GetNearestHistory(usr.Properties.Account)
            };
            return View("/Views/Game/FinanceServices/Banking.cshtml", model);
        }   

        public bool TestAccountExistence(string account)
        {
            return bankRep.CheckAccountExistence(account);
        }

        [VerifyUser]
        public JsonResult PaymentOrder(string counterpart, int amount, int vs, string message)
        {
            User usr = getLoggedUser(userRep, false);
            if(amount <= 0 || usr.Properties.Account.Balance < amount) return null;
            if(!bankRep.CheckAccountExistence(counterpart)) return null;
            bankRep.UpdateBalance(usr.Properties.Account, -1 * amount, false);
            Account counterpartAccount = bankRep.UpdateBalance(counterpart, amount, false);
            bankRep.SaveHistory(usr.Properties.Account, counterpart, usr.Properties.Account.Balance + amount, vs, message, false);
            bankRep.SaveHistory(counterpartAccount, usr.Properties.Account.AccountNumber, counterpartAccount.Balance - amount, vs, message, false);
            FBSS.SaveChanges();
            return new JsonResult(new {balance=usr.Properties.Account.Balance, history = new {date = DateTime.Now.Date.ToString("dd-MM-yyyy"), change = -1*amount}});
        }

        //GET: "server"/Game/TransactionHistory
        [VerifyUser]
        public ActionResult TransactionHistory()
        {
            return TransactionHistory(DateTime.Now.AddMonths(-1).ToString("dd-MM-yyyy"), DateTime.Now.ToString("dd-MM-yyyy"));
        } 

        //POST: "server"/Game/TransactionHistory

        [HttpPost]
        [VerifyUser]
        public ActionResult TransactionHistory(string dFrom, string dTo)
        {
            User usr = getLoggedUser(userRep);
            DateTime dateFrom, dateTo;
            if(!DateTime.TryParseExact(dFrom, "dd-MM-yyyy", null, DateTimeStyles.None, out dateFrom) || !DateTime.TryParseExact(dTo, "dd-MM-yyyy", null, DateTimeStyles.None, out dateTo)) return RedirectToAction("Index", "Home");
            ViewBag.DateFrom = dFrom;
            ViewBag.DateTo = dTo;
            return View("/Views/Game/FinanceServices/TransactionHistory.cshtml", bankRep.GetHistory(usr.Properties.Account, dateFrom.AddSeconds(-1), dateTo.AddDays(1)));
        }

        //GET: "server"/Game/Investitions
        [VerifyUser]
        public ActionResult Investitions()
        {
            var usr = getLoggedUser(userRep);
            SavingsModel model = new SavingsModel(){
                SavingsAccounts = usr.Properties.Account.SavingsAccounts,
                Fonds = usr.Properties.Account.Fonds
            };
            return View("/Views/Game/FinanceServices/Investitions.cshtml", model);
        }

        public JsonResult GetFondDescription(int fondType)
        {
            FondType type = bankRep.GetFondType(fondType);
            if(type == null) return null;
            return new JsonResult(new {
                minInterest = type.MinimalInterest,
                maxInterest = type.MaximalInterest,
                interestProbability = type.InterestProbability
            });
        }

        [HttpPost]
        [VerifyUser]
        public ActionResult CreateFond(int fondType, int investedMoney)
        {
            User usr = getLoggedUser(userRep, false);
            if(!bankRep.ValidateFondType(fondType) 
            || !bankRep.CreateFond(fondType, investedMoney, usr.Properties.Account, actionRep))
            {
                Danger("There were some mistakes during creating fond. Try to refresh site and create again");
            }            
            return RedirectToAction("Investitions");
        }  

        [HttpPost]
        [VerifyUser]
        public ActionResult CollectFondMoney(string creationTimeString)
        {
            User usr = getLoggedUser(userRep, false);
            if(!bankRep.CollectFromFond(usr.Properties.Account, creationTimeString, actionRep)){
                Danger("Some error occurred during collecting money. Try to refresh site and collect again");
            }
            return RedirectToAction("Investitions");
        }

        public JsonResult GetSavingsAccountDescription(int accountType)
        {
            SavingsAccountType type = bankRep.GetSavingsAccountType(accountType);
            if(type == null) return null;
            return new JsonResult(new {
                minDuration = type.MinimalDuration,
                maxDuration = type.MaximalDuration,
                minFee = type.MinimalFee,
                maxFee = type.MaximalFee,
                withdrawalFee = type.WithdrawalFee,
                minInterest = Math.Round(Math.Min(SavingsAccount.GetInterest(type, type.MinimalFee, type.MinimalDuration), SavingsAccount.GetInterest(type, type.MinimalFee, type.MaximalDuration)), 2),
                maxInterest = Math.Round(type.GetMaxInterest(),2),
                minSubsidy  = Math.Round(SavingsAccount.GetSubsidy(type, type.MinimalFee, type.MinimalDuration),2),
                maxSubsidy  = Math.Round(SavingsAccount.GetSubsidy(type, type.MaximalFee, type.MaximalDuration),2)
            });
        }

        public JsonResult GetActualSavingsAccountDescription(int accountType, int monthlyFee, int duration)
        {
            SavingsAccountType type = bankRep.GetSavingsAccountType(accountType);
            if(type == null) return null;
            if(type.CheckValidFeeAndDuration(accountType, monthlyFee)) return null;
            return new JsonResult(new {
               interest = Math.Round(SavingsAccount.GetInterest(type, monthlyFee, duration), 2),
               subsidy = Math.Round(SavingsAccount.GetSubsidy(type, monthlyFee, duration), 2),
               withdrawalFee = type.WithdrawalFee
            });
        }

        [HttpPost]
        [VerifyUser]
        public ActionResult CreateSavingsAccount(int accountType, int monthlyFee, int duration)
        {
            User usr = getLoggedUser(userRep, false);
            SavingsAccountType type = bankRep.GetSavingsAccountType(accountType);
            if(type == null || !type.CheckValidFeeAndDuration(monthlyFee, duration)) {
                Danger("We have got some invalid information, please refresh your site and try again");
                return RedirectToAction("Investitions");
            }
            if(!bankRep.CreateSavingsAccount(usr, accountType, monthlyFee, duration, actionRep)){
                Danger("During creating your account an error occurs, please refresh your site and try again");
            }
            return RedirectToAction("Investitions");
        }

        [HttpPost]
        [VerifyUser]
        public ActionResult WithdrawSavingsAccount(string creationTime)
        {
            User usr = getLoggedUser(userRep, false);
            SavingsAccount account = bankRep.GetSavingsAccount(usr.Properties.AccountNumber, creationTime);
            if(account == null)
            {
                Danger("Your account was not found");
                return RedirectToAction("Investitions");
            }
            if(account.Account.Balance + account.InvestedMoney < account.AccountType.WithdrawalFee)
            {
                Danger("You do not have enough money to pay withdrawal fee");
                return RedirectToAction("Investitions");
            }
            bankRep.WithdrawSavingsAccount(account, actionRep);
            return RedirectToAction("Investitions");
        }

        //GET: "server"/Game/Insurance
        [VerifyUser]
        public ActionResult Insurance()
        {
            var usr = getLoggedUser(userRep);
            return View("/Views/Game/FinanceServices/Insurance.cshtml", usr.Properties.Account);
        }

        [VerifyUser]
        public double GetLifeInsuranceMonthlyFee(double insuredMoney)
        {
            User usr = getLoggedUser(userRep, false);
            if(usr == null) return double.NaN;
            return Math.Round(LifeInsurance.CountMonthlyFee(insuredMoney, 0 /*TODO -> motorgage*/, usr), 2);
        }

        [VerifyUser]
        public JsonResult GetInvalidityInsuranceMonthlyFee(double insuredMoney, int type){
            User usr = getLoggedUser(userRep, false);
            if(usr == null || !Enum.IsDefined(typeof(InvalidityInsurance.Types), type)) return null;
            return new JsonResult(new{ fee=Math.Round(InvalidityInsurance.CountMonthlyFee(insuredMoney, usr, (InvalidityInsurance.Types)type), 2), creation=InvalidityInsurance.CreationFee});
        }

        [VerifyUser]
        [HttpPost]
        public ActionResult CreateInvalidityInsurance(double insuredMoney, int type)
        {
            User usr = getLoggedUser(userRep, false);
            if(usr.Properties.Account.Balance < InvalidityInsurance.CreationFee){
                Danger("You do not have enough money to pay fee fro creation");
                return RedirectToAction("Insurance");
            }
            if(usr == null
              || usr.Properties.Account.InvalidityInsurance != null
              || !InvalidityInsurance.ValidateInsuredMoney(insuredMoney)
              || !Enum.IsDefined(typeof(InvalidityInsurance.Types), type)
              || !bankRep.CreateInvalidityInsurance(usr, insuredMoney, type, actionRep))
            {
                Danger("An error occured during creating insurance, try to refresh site and create it again");
            }            
            return RedirectToAction("Insurance");
        }

        //GET: "server"/Game/Family
        [VerifyUser]
        public ActionResult Family()
        {
            User usr = getLoggedUser(userRep);
            FamilyModel model = new FamilyModel(){
                HouseholdExpenditure = usr.Properties.HouseholdExpenditure,
                NextHouseholdExpenditurePayment = BasicUtils.GetNextDayOfWeek(UserUtils.CountVirtualDate(usr.TimeOfCreation, usr.Properties.VirtualDayDuration), DayOfWeek.Monday)
            };
            return View(model);
        }

        [HttpPost]
        [VerifyUser]
        public ActionResult ChangeHouseHoldExpenditure(int value)
        {
            User usr = getLoggedUser(userRep, false);
            if(!bankRep.ChangeHouseholdExpenditure(usr, value)) Danger("Your new household expenditure hasn't been changed, try to refresh site and change again");
            return RedirectToAction("Family");
        }

        //GET: "server"/Game/Job
        [VerifyUser]
        public ActionResult Job()
        {
            User usr = getLoggedUser(userRep);
            return View();
        }

        //GET: "server"/Game/Life
        [VerifyUser]
        public ActionResult Life()
        {
            User usr = getLoggedUser(userRep);
            return View("/Views/Game/LifeAndProperty/Life.cshtml");
        }

        //GET: "server"/Game/HappinessHistory
        [VerifyUser]
        public ActionResult HappinessHistory()
        {
            User usr = getLoggedUser(userRep);
            return View("/Views/Game/LifeAndProperty/HappinessHistory.cshtml");
        }

        //GET: "server"/Game/EventsHistory
        [VerifyUser]
        public ActionResult EventsHistory()
        {
            User usr = getLoggedUser(userRep);
            return View("/Views/Game/LifeAndProperty/EventsHistory.cshtml");
        }
    }
}