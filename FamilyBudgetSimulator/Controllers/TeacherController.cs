using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FamilyBudgetSimulator.Models;

namespace FamilyBudgetSimulator.Controllers
{
    public class TeacherController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Overview()
        {
            return View("/Views/Teacher/Overview/ClassOverview.cshtml");
        }    

        public IActionResult Administration()
        {
            return View();
        }       
    }
}
