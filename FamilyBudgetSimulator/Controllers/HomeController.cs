﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using FamilyBudgetSimulator.Models;
using Microsoft.EntityFrameworkCore;
using DatabaseRepositories;
using DatabaseModels;
using Utils;
using FamilyBudgetSimulator.Attributes;

namespace FamilyBudgetSimulator.Controllers
{
    public class HomeController : BaseController
    {
        private readonly FBSDbContext FBSS;
        private readonly CareerRepository CareerRep;
        private readonly UserRepository UserRep;
        private readonly BankingRepository BankingRep;

        public HomeController(FBSDbContext context)
        {
            FBSS = context;
            UserRep = new UserRepository(FBSS);
            CareerRep = new CareerRepository(FBSS);
            BankingRep = new BankingRepository(FBSS);
        }

        public ActionResult Index()
        {
            User user = getLoggedUser(UserRep);
            if(user == null) return RedirectToAction("Login");
            if(user.Career == null) return RedirectToAction("CreateCareer");
            if(user.RoleId == Roles.FreeUser || user.RoleId == Roles.PaiedUser) return RedirectToAction("Index", "Game");   
            if(user.RoleId == Roles.Teacher) return RedirectToAction("Index", "Teacher");
            if(user.RoleId == Roles.Student) throw new NotImplementedException();
            throw new ApplicationException("Unknown user role");        
        }        

        public ActionResult UserSettings()
        {
            return View();
        }

        public ActionResult Login()
        {
            if(TestUserInCookies())
            {
                logInUser(getUserInCookies(UserRep));
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Login(string name, string password, bool remember)
        {
            if(!UserUtils.VerifyUserAttributes(name, password: password))
            {
                Danger("Your log in information are in incorrect format");
                return RedirectToAction("Login");
            }
            User usr = UserRep.TestLoginPassword(name, password);
            if(usr == null)
            {
                Danger("Invalid user name or password");
                return RedirectToAction("Login");
            }
            logInUser(usr);
            if(remember)
                setCookie(ApplicationConstants.USER_COOKIE_KEY, usr.Id.ToString());
            return RedirectToAction("Index");            
        }

        [HttpPost]
        public ActionResult Register(string userName, string password, string email, string token, string accountType)
        {             
            if(!UserUtils.VerifyUserAttributes(userName, email, password) || !UserRep.TestEmailAndLoginUnique(userName, email) || !UserUtils.CheckEmailToken(token, email))
            {
                Danger("Your log in information are in incorrect format");
                return RedirectToAction("Login");
            }
            User usr;
            if((usr = UserRep.CreateUser(userName, email, password)) != null)
            {
                if(accountType == "Premium") return RedirectToAction("Login"); //TODO payment
                logInUser(usr);
                return RedirectToAction("Index");
            }
            Danger("Unable to create this user, try to refresh site and sign-up again");
            return RedirectToAction("Login");
        }

        [HttpPost]
        public ActionResult LogOut()
        {
            HttpContext.Session.Remove(ApplicationConstants.LOGGED_IN_SESSION_KEY);
            HttpContext.Session.Remove(ApplicationConstants.LOGGED_ROLE_SESSION_KEY);
            Response.Cookies.Delete(ApplicationConstants.USER_COOKIE_KEY);
            return RedirectToAction("Login");
        }

        private void logInUser(User usr)
        {
            HttpContext.Session.SetInt32(ApplicationConstants.LOGGED_IN_SESSION_KEY, usr.Id);
            HttpContext.Session.SetInt32(ApplicationConstants.LOGGED_ROLE_SESSION_KEY, usr.RoleId);
            UserRep.UpdateLastLogin(usr);
        }

        [VerifyUser]
        public ActionResult CreateCareer()
        {
            User usr = getLoggedUser(UserRep);
            if(usr.Career != null) return RedirectToAction("Index");
            return View();
        }

        [HttpPost]
        [VerifyUser]
        public ActionResult CreateCareer(int CareerTypeId, int virtualMonthDuration)
        {
            User usr = getLoggedUser(UserRep);
            if(usr.Career != null) return RedirectToAction("Index");
            if(!CareerTypeId.IsBetween(0, CareerTypes.All.Length)) CareerTypeId = BasicUtils.GenRandomNumber(0, CareerTypes.All.Length);
            if(virtualMonthDuration <= 0) virtualMonthDuration = 20;
            if(!UserRep.CreateUserProperties(usr, CareerTypeId, virtualMonthDuration, BankingRep) ||
               !CareerRep.CreateCareer(usr, CareerTypeId))
            {
                Danger("Unable to create your career. Try to refresh site or create new user");
                return RedirectToAction("CreateCareer");
            }
            return RedirectToAction("Index");
        }

        public bool CheckUserNameUnique(string login)
        {
            return UserRep.TestUserUnique(login);
        }

        public bool CheckEmailUnique(string email)
        {
            return UserRep.TestEmailUnique(email);
        }

        public bool CheckToken(string token, string email)
        {
            return UserUtils.CheckEmailToken(token, email);
        } 

        public string PostComfrimationEmail(string email)
        {
            string token = UserUtils.GenerateEmailToken(email);
            //TODO -> postEmail
            return token;
        }

        public ActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
