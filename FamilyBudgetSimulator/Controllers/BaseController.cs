using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using FamilyBudgetSimulator.Models;
using DatabaseModels;
using DatabaseRepositories;
using Utils;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Newtonsoft.Json;

namespace FamilyBudgetSimulator.Controllers
{
    public class BaseController : Controller
    {       
        public User getLoggedUser(UserRepository rep, bool setViewBag = true)
        {
            int? userId = HttpContext.Session.GetInt32(ApplicationConstants.LOGGED_IN_SESSION_KEY);
            if(userId == null) return null;
            User usr = rep.GetUser((int)userId);
            if(setViewBag)
            {
                TempData["UserName"] = usr.Login;
                TempData["UserEmail"] = usr.Email;
                TempData["UserRole"] = usr.Role.Description;
                if(usr.Career != null) TempData["VirtualDate"] = UserUtils.CountVirtualDate(usr.TimeOfCreation, usr.Properties.VirtualDayDuration).ToString("dd-MM-yyyy");
                if(usr.Career != null) TempData["UserCareer"] = usr.Career.CareerType.Description;
            }
            return usr;
        }

        public User getUserInCookies(UserRepository rep)
        {
            if(!TestUserInCookies()) return null;
            int id = int.Parse(Request.Cookies[ApplicationConstants.USER_COOKIE_KEY].ToString());
            return rep.GetUser(id);
        }

        public bool TestUserInCookies()
        {
            return Request.Cookies[ApplicationConstants.USER_COOKIE_KEY] != null;
        }

        public void setCookie(string cookieName,string cookieValue, int duration = 15768000)
        {
            CookieOptions opt = new CookieOptions(){
                Expires = DateTime.Now.AddSeconds(duration)
            };
            if(Request.Cookies[cookieName] != null) Response.Cookies.Delete(cookieName);
            Response.Cookies.Append(cookieName, cookieValue, opt);
        }
        
        #region Alerts
        public void Success(string message, bool dismissable = true)
        {
            AddAlert(AlertStyles.Success, message, dismissable);
        }

        public void Information(string message, bool dismissable = true)
        {
            AddAlert(AlertStyles.Information, message, dismissable);
        }

        public void Warning(string message, bool dismissable = true)
        {
            AddAlert(AlertStyles.Warning, message, dismissable);
        }

        public void Danger(string message, bool dismissable = true)
        {
            AddAlert(AlertStyles.Danger, message, dismissable);
        }

        private void AddAlert(string alertStyle, string message, bool dismissable)
        {
            var alerts = TempData.ContainsKey(Alert.TempDataKey)
                ? (List<Alert>)TempData.Get<List<Alert>>(Alert.TempDataKey)
                : new List<Alert>();

            alerts.Add(new Alert
            {
                AlertStyle = alertStyle,
                Message = message,
                Dismissable = dismissable
            });

            TempData.Put<List<Alert>>(Alert.TempDataKey,alerts);
        }
        #endregion
    }

    public static class TempDataExtensions
    {
        public static void Put<T>(this ITempDataDictionary tempData, string key, T value) where T : class
        {
            tempData[key] = JsonConvert.SerializeObject(value);
        }

        public static T Get<T>(this ITempDataDictionary tempData, string key) where T : class
        {
            object o;
            tempData.TryGetValue(key, out o);
            return o == null ? null : JsonConvert.DeserializeObject<T>((string)o);
        }
    }
}