using System;
using System.Collections.Generic;
using System.Linq;

namespace FamilyBudgetSimulator.Models
{
    public class BankingModel
    {
        public String AccountNumber {get;set;}
        public double Balance {get; set;}
        private Tuple<DateTime, double>[] nearestHistory;
        public Tuple<DateTime, double>[] NearestHistory{
            get{
                return nearestHistory;
            }
            set{
                if(value.Length < 3){
                    List<Tuple<DateTime, double>> help = value.ToList();
                    for(int i = 0; i < 3 - value.Length; i++)
                    {
                        help.Add(new Tuple<DateTime,double>(DateTime.MinValue, 0));
                    }
                    nearestHistory = help.ToArray();
                }
                else nearestHistory = value;
            }
        }

        //TODO Loans
    }
}