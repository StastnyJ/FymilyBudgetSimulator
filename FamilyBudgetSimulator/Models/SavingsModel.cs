using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseModels;

namespace FamilyBudgetSimulator.Models
{
    public class SavingsModel
    {
        public List<SavingsAccount> SavingsAccounts {get;set;}

        public List<Fond> Fonds {get;set;}
    }
}