using System;
using System.Collections.Generic;
using System.Linq;

namespace FamilyBudgetSimulator.Models
{
    public class FamilyModel
    {
       public int HouseholdExpenditure {get;set;}
       public DateTime NextHouseholdExpenditurePayment {get;set;}
    }
}