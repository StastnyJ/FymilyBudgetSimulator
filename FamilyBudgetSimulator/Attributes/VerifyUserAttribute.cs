using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Http;
using System.Linq;
using Utils;

namespace FamilyBudgetSimulator.Attributes
{
    public class VerifyUserAttribute : ActionFilterAttribute
    {
        private  int[] roles;
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            int? role = context.HttpContext.Session.GetInt32(ApplicationConstants.LOGGED_ROLE_SESSION_KEY);
            if(role == null)
            {
                context.Result = new RedirectResult("/Home/Login");
            }
            else if(!roles.Contains((int)role))
            {
                context.Result = new RedirectResult("/Home/Index");
            }
        }

        public VerifyUserAttribute(params int[] roles)
        {
            if(roles == null || roles.Length == 0) roles = DatabaseModels.Roles.All;
            this.roles = roles;
        }
    }
}