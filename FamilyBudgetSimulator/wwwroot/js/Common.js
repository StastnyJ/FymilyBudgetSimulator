function SetValid(element)
{
    element.removeClass("is-invalid");
    element.addClass("is-valid");
}

function SetInvalid(element, messageElement, message)
{
    messageElement.text(message);
    element.removeClass("is-valid");
    element.addClass("is-invalid");
}

function CountPasswordStrength(pass) 
{
    var score = 0;
    if (!pass)
        return score;

        var letters = new Object();
    for (var i=0; i<pass.length; i++) {
        letters[pass[i]] = (letters[pass[i]] || 0) + 1;
        score += 5.0 / letters[pass[i]];
    }

    var variations = {
        digits: /\d/.test(pass),
        lower: /[a-z]/.test(pass),
        upper: /[A-Z]/.test(pass),
        nonWords: /\W/.test(pass),
    }

    variationCount = 0;
    for (var check in variations) {
        variationCount += (variations[check] == true) ? 1 : 0;
    }
    score += (variationCount - 1) * 10;

    return parseInt(score);
}

function LoadingButton(button)
{
    var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> loading...';
    if (button.html() !== loadingText) {
      button.data('original-text', button.html());
      button.html(loadingText);
      button.addClass("disabled");
      button.attr('disabled','disabled');
    }
}

function ResetButton(button)
{
    origin = button.data('original-text');
    button.html(origin);
    button.removeClass("disabled");
    button.removeAttr('disabled','disabled');
}

function post(path, params, method) {
    method = method || "post";

    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}

function disable(element){
    element.addClass("disabled");
    element.attr("disabled", "disabled");
}

function enable(element){
    element.removeClass("disabled");
    element.removeAttr("disabled");
}

function isNull(value){
    return value === null || value === undefined;
}

function isNumber(str){
    return $.isNumeric(str);
}

function isNormalInteger(str) {
    if(isNull(str)) return false;
    var n = Math.floor(Number(str));
    return n !== Infinity && String(n) === str;
}

function getVirtualDate(){
    var virtualDateString = $("#virtualDate").html().split('-');
    return new Date(virtualDateString[2], virtualDateString[1] - 1, virtualDateString[0]);
}

function formatNumber(number, digits){
    return ("0".repeat(digits) + number).slice(-1 * Math.max(number.toString().length, digits));
}

function formatDate(d){
    return formatNumber(d.getDate(), 2) + "-" + formatNumber(d.getMonth() + 1,2) + "-" + formatNumber(d.getFullYear(), 4)
}

function sleep(delay) {
    var start = new Date().getTime();
    while (new Date().getTime() < start + delay);
}

function initializeDateOrderForDataTables()
{
    jQuery.extend(jQuery.fn.dataTableExt.oSort, {
        "extract-date-pre": function(value) {
            var date = value;
            date = date.split('-');
            return Date.parse(date[1] + '/' + date[0] + '/' + date[2])
        },
        "extract-date-asc": function(a, b) {
            return ((a < b) ? -1 : ((a > b) ? 1 : 0));
        },
        "extract-date-desc": function(a, b) {
            return ((a < b) ? 1 : ((a > b) ? -1 : 0));
        }
    });
}

function numberToStringSeparateThousands(n){
        var parts=n.toString().split(".");
        return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ") + (parts[1] ? "." + parts[1] : "");
}