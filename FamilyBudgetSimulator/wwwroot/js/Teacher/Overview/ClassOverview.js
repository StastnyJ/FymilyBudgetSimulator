function randomArray(length, max, min) {
    return Array.apply(null, Array(length)).map(function() {
        return Math.round(Math.random() * (max - min) + min);
    });
}
function getXAxe()
{
    var res = []
    for(var i = 1; i <= 31; i++)
    {
        res.push('2018-03-' + ("0" + i.toString()).slice(-2) + ' 00:00:00')
    }
    return res;
}
function genPlotData()
{
    var plotdata = [];
    for(var i = 0; i < 10; i++)
    {
        plotdata.push({
            x: getXAxe(),
            y: randomArray(31, 10000, 20000),
            type: 'scatter',
            name: "student-" + (2*(i + 1) - 1) + " + student-" + 2*(i + 1)
        })
    }
    return plotdata;
}

$(function(){
    $('#navbar_top_overview').addClass("active");
    $("#property-graph").css("width", $("#money-graph").css("width"))
    $("#happiness-graph").css("width", $("#money-graph").css("width"))
   
    Plotly.plot(document.getElementById("money-graph"), genPlotData(), {
        margin: { t: 28 }, legend: {orientation: "h"} } )
    Plotly.plot(document.getElementById("property-graph"), genPlotData(), {
        margin: { t: 28 }, legend: {orientation: "h"} } )

    Plotly.plot(document.getElementById("happiness-graph"), genPlotData(), {
        margin: { t: 28 }, legend: {orientation: "h"} } )
        
});