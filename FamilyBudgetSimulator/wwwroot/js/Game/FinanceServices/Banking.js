$("#Counterpart").focusout(function(){
    var element = $(this)
    text = element.val();
    if(!/^\d{3}-\d{10}[/]0\d00$/i.test(text) || text == $("Account").attr("account"))
    {
        SetInvalid(element, $("#CounterpartError"),"Invalid account format");
    }
    else
    {
        $.post("/Game/TestAccountExistence", {account: text}).done(function(data){
            if(data)
            {
                SetValid(element)
            }
            else
            {
                SetInvalid(element, $("#CounterpartError"), "This account does not exist")
            }
        });
    }
});

$("#Ammount").focusout(function(){
    if(isNormalInteger($(this).val()) && $(this).val() > 0 && $(this).val() <= parseInt($("#Balance").attr("balance"))){
        SetValid($(this))
    }
    else{
        SetInvalid($(this), $("#AmmountError"), "Insert positive number lower and equal to your balance")
    }
});

$("#VariableSymbol").focusout(function(){
    if(isNormalInteger($(this).val()) && $(this).val() > 0){
        SetValid($(this))
    }
    else{
        SetInvalid($(this), $("#VariableSymbolError"), "Insert positive number")
    }
})

$("#btnSendPayment").click(function(){
    if($("#Counterpart").hasClass("is-valid") && $("#Ammount").hasClass("is-valid") && $("#VariableSymbol").hasClass("is-valid"))
    {
        $.post("/Game/PaymentOrder", {counterpart: $("#Counterpart").val(), amount: $("#Ammount").val(), vs: $("#VariableSymbol").val(), message: $("#Message").val()}).done(function(data){
            $("#Balance").attr("balance", data.balance);
            $("#Balance").html("€ " + data.balance);
            $("#transactionHistory tr:last").remove();
            $("#transactionHistory").prepend("<tr><td>" + data.history.date +  '</td><td style="color: red">€ ' + data.history.change + '</td></tr>')
        });
    }
    else{
        alert("Invalid");
    }
})