getAcutalSavingsAccountDesc = function(id, fee, duration){
    $.post("/Game/GetActualSavingsAccountDescription", {accountType: id, monthlyFee: fee, duration: duration}).done(function(result){
        $("#savingsCreateActDescFee").val("€ " + fee);
        $("#savingsCreateActDescDuration").val(duration + " months");
        $("#savingsCreateActDescInterest").val(result.interest + " %");
        $("#savingsCreateActDescSubsidy").val("€ " + result.subsidy);
        $("#savingsCreateActDescWithdrawalFee").val("€ " + result.withdrawalFee);
        $("#savingsCreateActDesInvested").val("€ " + (fee * duration));
        $("#savingsCreateActDescIncome").val("€ " + Math.floor(result.subsidy + (fee * duration * Math.pow(1 + (result.interest / 100), Math.floor(duration / 12)))));

        var virtualDate = getVirtualDate();
        virtualDate.setMonth(virtualDate.getMonth() + parseInt(duration));
        $("#savingsCreateActDescDueDate").val(formatDate(virtualDate));
    });
}

getSavingsAccountDesc = function(id){
    $.post("/Game/GetSavingsAccountDescription", {accountType: id}).done(function(result){
        $("#savingsCreateDescMinDuration").val(result.minDuration + " months");
        $("#savingsCreateDescMaxDuration").val(result.maxDuration + " months");
        $("#savingsCreateDescInterest").val(result.minInterest + " - " + result.maxInterest + " %");
        $("#savingsCreateDescSubsidy").val("€ " + result.minSubsidy + " - " + result.maxSubsidy);
        $("#savingsCreateDescMinFee").val("€ " + result.minFee);
        $("#savingsCreateDescMaxFee").val("€ " + result.maxFee);
        $("#savingsCreateDescWithdrawFee").val("€ " + result.withdrawalFee);

        $("#SavingsCreateMonthlyFee").attr("min", result.minFee);
        $("#SavingsCreateMonthlyFee").attr("max", result.maxFee);
        $("#SavingsCreateTypeDuration").attr("min", result.minDuration);
        $("#SavingsCreateTypeDuration").attr("max", result.maxDuration);
    });
}
getSavingsAccountDesc(0);

checkRangeValidity = function(element){
    return isNormalInteger(element.val()) && parseInt(element.val()) >= parseInt(element.attr("min")) && parseInt(element.val()) <= parseInt(element.attr("max"));
}

validateSavingsCreateTextBoxes = function(){
    var feeBox = $("#SavingsCreateMonthlyFee");
    var durationBox = $("#SavingsCreateTypeDuration");
    var ok = true
    if(checkRangeValidity(feeBox))
    {
        SetValid(feeBox)
    }
    else
    {
        ok = false
        SetInvalid(feeBox, $("#SavingsCreateMonthlyFeeError"), "Insert integer in the allowed range")
    }
    if(checkRangeValidity(durationBox))
    {
        SetValid(durationBox);
    }
    else{
        SetInvalid(durationBox, $("#SavingsCreateTypeDurationError"), "Insert integer in the allowed range");
        ok = false
    }
    return ok;
}

getFondDescription = function(id){
    $.post("/Game/GetFondDescription", {fondType: id}).done(function(data){
        $("#fondCreateDescInterestFrom").val(data.minInterest * 100 + " %");
        $("#fondCreateDescInterestTo").val(data.maxInterest * 100 + " %");
        createFondProbabilityChart(data.interestProbability);
    });
}
getFondDescription(0);

createFondProbabilityChart = function(data){
    $("#createFondInterestProbability").html('<div id="createFondInterestProbabilityChartBody"></div>');
    var xAxe = []
    var yAxe = []
    for(i in data)
    {
        xAxe.push(data[i].item1 * 100);
        xAxe.push(data[i].item2 * 100);
        yAxe.push(data[i].item3);
        yAxe.push(data[i].item3);
    }
    Plotly.newPlot('createFondInterestProbabilityChartBody', [{
        x: xAxe,
        y: yAxe,
        mode: 'lines',
        type: 'scatter'
    }]);
}

$("#SavingsCreateTypeSelect").change(function(){
    getSavingsAccountDesc($(this).val());
    if(validateSavingsCreateTextBoxes()) getAcutalSavingsAccountDesc($(this).val(), $("#SavingsCreateMonthlyFee").val(), $("#SavingsCreateTypeDuration").val());
});

$("#SavingsCreateMonthlyFee").focusout(function(){
    if(validateSavingsCreateTextBoxes()) getAcutalSavingsAccountDesc($("#SavingsCreateTypeSelect").val(), $(this).val(), $("#SavingsCreateTypeDuration").val());
});

$("#SavingsCreateTypeDuration").focusout(function(){
    if(validateSavingsCreateTextBoxes()) getAcutalSavingsAccountDesc($("#SavingsCreateTypeSelect").val(), $("#SavingsCreateMonthlyFee").val(), $(this).val());
});

$("#btnCreateSavingsAccount").click(function(){
    disable($(this));
    if(validateSavingsCreateTextBoxes())
    {
        post("/Game/CreateSavingsAccount", {accountType: $("#SavingsCreateTypeSelect").val(), monthlyFee: $("#SavingsCreateMonthlyFee").val(), duration: $("#SavingsCreateTypeDuration").val()});
    }
    enable($(this));
});

$(".btnWithdrawSavingsAccount").click(function(){
    $("#WithdrawSavingsAccountFee").html("€ " + $(this).attr("withdrawalFee"));
    $("#btnWithdrawSavingsAccountConfirmation").attr("creationTime", $(this).attr("creationTime"));
    $("#WithdrawSavingAccountModal").modal("show");
});

$("#btnWithdrawSavingsAccountConfirmation").click(function(){
    post("/Game/WithdrawSavingsAccount", {creationTime: $(this).attr("creationTime")});
});

$("#fondCreateTypeSelect").click(function(){
    getFondDescription($(this).val());
});

$("#btnCreateInvestitionFond").click(function(){
    var amountOfMoney = $("#FondCreateMoney").val();
    if(!isNormalInteger(amountOfMoney) || amountOfMoney <= 0){
        SetInvalid($("#FondCreateMoney"), $("#FondCreateMoneyError"), "Amount of invested money must be positive integer");
        return;
    }
    post("/Game/CreateFond", {fondType: $("#fondCreateTypeSelect").val(), investedMoney: amountOfMoney});
});

$(".btnInvestitionFondCollect").click(function(){
    post("/Game/CollectFondMoney", {creationTimeString: $(this).attr("creationDate")});
});