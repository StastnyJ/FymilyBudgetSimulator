initializeDateOrderForDataTables();

$("#source-tab").one("click", function(){
    setTimeout(function(){
        var table = $('#historyTable').DataTable({
            paging: false,
            bsort: false,
            scrollY: "60vh",
            scrollCollapse: true,   
            info: false,
            dom: "t",
            columnDefs: [{
                type: 'extract-date',
                targets: [0]
            }],
            order: [[0,"desc"]]
        });
    }, 250);
});

$('#dateFrom').datetimepicker({
    format: "DD-MM-YYYY",
});

$("#dateFrom").val($("#dateFrom").attr("value"))

$('#dateTo').datetimepicker({
    format: "DD-MM-YYYY",
});

$("#dateTo").val($("#dateTo").attr("value"));

plotData = getPlotData();
Plotly.newPlot('presentation', [{
    x: plotData.x,
    y: plotData.y,
    type: 'scatter'
}],{
    yaxis:{
        tick0:0
    }
});    

function getPlotData(){
    res = {x : [], xData: [], y: []}
    $("#historyTable>tbody>tr").each(function(index){
        var date = $(this).attr("date")
        res.xData.unshift(date);
        res.x.unshift(getPercentageDate(date));
        res.y.unshift($(this).attr("balance"));
        res.xData.unshift(date);
        res.x.unshift(getPercentageDate(date));
        res.y.unshift($(this).attr("oldbalance"));
    });
    res.xData.unshift("start");
    res.x.unshift(0);
    res.y.unshift(res.y[0]);
    res.xData.push("end");
    res.x.push(100);
    res.y.push(res.y[res.y.length - 1]);
    console.log(res.x);
    console.log(res.y);
    return res;
}
function getPercentageDate(dateS)
{
    var startDateString = $("tbody").attr("firstDate").split("-");
    var start = new Date(startDateString[2], startDateString[1], startDateString[0], startDateString[3], startDateString[4], startDateString[5]);
    var endDateString = $("tbody").attr("lastDate").split("-");
    var end = new Date(endDateString[2], endDateString[1], endDateString[0], endDateString[3], endDateString[4], endDateString[5]);
    dateString = dateS.split("-");
    var date = new Date(dateString[2], dateString[1], dateString[0], dateString[3], dateString[4], dateString[5]);
    var diff = end.getTime() - start.getTime();
    var actDiff = date.getTime() - start.getTime();
    return 100 * actDiff / diff;
}