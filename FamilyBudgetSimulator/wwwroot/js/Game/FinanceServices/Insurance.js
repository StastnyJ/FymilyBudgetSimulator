$(".slider").slider().on("slide", function(a){
    $("#InsuranceHightVal").html(a.value + " %");   
});
$(".slider.slider-horizontal").css("width", "100%");

$("#lifeInsuranceMoneySlider").on("slide", function(a){
    $("#lifeInsuranceMoney").html("€ " + numberToStringSeparateThousands(a.value));
    calculateLifeInsurance();
});

$("#lifeInsuranceMoneySlider").on("slideStop", function(a){
    if(!isNumber($(this).val())) {
        $("#lifeInsuranceMonthlyFee").html("");
        return;
    }
    $.post("/Game/GetLifeInsuranceMonthlyFee", {insuredMoney: $(this).val()}).done(function(res){
        $("#lifeInsuranceMonthlyFee").html("€ " + res)
    });
});

$("#lifeInsuranceCalcAgeSlider").on("slide", function(a){
    $("#lifeInsuranceCalcAge").html(a.value + " years");
    calculateLifeInsurance();
});

$("#invalidityInsuranceCalcAgeSlider").on("slide", function(a){
    $("#invalidityInsuranceCalcAge").html(a.value + " years");
    calculateInvalidityInsurance();
});

$("#invalidityInsuranceSumSlider").on("slide", function(a){
    $("#invalidityInsurancSum").html("€ " + numberToStringSeparateThousands(a.value));
    calculateInvalidityInsurance();
});

$("#invalidityInsuranceSumSlider").on("slideStop", function(a){
    getActualInvalidityInsuranceFee();
});

$(".invalidityInsuranceDegreeSelect").on("change", function(a){
    getActualInvalidityInsuranceFee();
    calculateInvalidityInsurance();
});

$("#lifeInsuranceCalcMortgage").on("focusout", function(){calculateLifeInsurance()});

$("#createLifeInsurance").click(function(){
    var insuredMoney = $("#lifeInsuranceMoney").html().substring(2).replace(/\s/g,'');
    if(!isNumber(insuredMoney)){
        return;
    }
    post("/Game/CreateLifeInsurance", {insuredMoney: insuredMoney});
});

$("#createInvalidityInsurance").click(function(){
    var money = $("#invalidityInsurancSum").html().substring(2).replace(/\s/g,'');
    var type = getInvalidityInsuranceSelectedType();
    if(!isNumber(money) || type == 0){
        return;
    }
    post("/Game/CreateInvalidityInsurance", {insuredMoney: money, type: type});
});

function calculateLifeInsurance(){
    var age = parseInt($("#lifeInsuranceCalcAge").html().split(' ')[0]);
    var mortgage = $("#lifeInsuranceCalcMortgage").val();
    var insuredMoney = $("#lifeInsuranceMoney").html().substring(2).replace(/\s/g,'');
    if(!isNumber(mortgage) || !isNumber(insuredMoney)){
        $("#lifeInsuranceCalcResult").html("Monthly fee: -");
        return;
    }
    mortgage = parseFloat(mortgage);
    insuredMoney = parseFloat(insuredMoney);
    var fee = 0.08 + 2500 * (0.000204 - 0.00000572 * age + 0.0000026 * age * age - 0.000000114 * age * age * age + 0.00000000459 * age * age * age * age) * (0.00000000004 * insuredMoney * insuredMoney +  0.0001 * mortgage);
    fee = Math.round(fee * 100) / 100;
    $("#lifeInsuranceCalcResult").html("Monthly fee: " + fee + " €");
}

function getActualInvalidityInsuranceFee(){
    var money = $("#invalidityInsurancSum").html().substring(2).replace(/\s/g,'');
    var type = getInvalidityInsuranceSelectedType();
    if(!isNumber(money) || type == 0){
        $("#InvalidityInsuranceMonthlyFee").html("");
        return;
    }
    $.post("/Game/GetInvalidityInsuranceMonthlyFee", {insuredMoney: money, type: type}).done(function(data){
        $("#InvalidityInsuranceMonthlyFee").html("€ " + data.fee + '<br><small style="font-size:initial !important">+ € ' + data.creation + ' creation fee</small>');
    });
}

function getInvalidityInsuranceSelectedType(){
    if($("#InvalidityInsuranceFirstDegree").is(":checked")) return 1;
    if($("#InvalidityInsuranceSecondDegree").is(":checked")) return 2;
    if($("#InvalidityInsuranceThirdDegree").is(":checked")) return 3;
    return 0;
}

function calculateInvalidityInsurance(){
    var age = parseInt($("#invalidityInsuranceCalcAge").html().split(' ')[0]);
    var insuredMoney =  $("#invalidityInsurancSum").html().substring(2).replace(/\s/g,'');
    var type = getInvalidityInsuranceSelectedType();
    if(type == 0 || !isNumber(insuredMoney)){
        $("#lifeInsuranceCalcResult").html("Monthly fee: -");
        return;
    }
    insuredMoney = parseFloat(insuredMoney);
    var feeSecondPart = type == 3 ? 15 * age - 100 : type == 2 ? 25 * age - 100 : age * age - 40 * age + 1000;
    var fee = (0.000000000006825 * insuredMoney * insuredMoney + 0.028) * feeSecondPart;
    fee = Math.round(fee * 100) / 100;
    $("#invalidityInsuranceCalcFee").html("Monthly fee: " + fee + " €");
}