$(function(){
    $('#navbar_top_family').addClass("active");

    $('.holidayRow').click(function() {
        $('#HolidayOverviewModal').modal('show');
    })
    
    $('#toggleone').bootstrapToggle();

    $('#dateFrom').datetimepicker({
        format: "DD-MM-YYYY",
    });

    $("#dateFrom").val($("#dateFrom").attr("value"))

    $('#dateTo').datetimepicker({
        format: "DD-MM-YYYY",
    });

    $("#dateTo").val($("#dateTo").attr("value"))

    var holidayTable = $('#BookHolidayTable').DataTable({
        paging: false,
        scrollY: "30vh",
        info: false,
        dom: "t",
    });
    
    $("tr.selectable").click(function() {
        if($(this).hasClass("selected"))
        {
            $(this).removeClass("selected")
            $(this).removeClass("table-info")
        }
        else
        {
            $("tr.selectable").each(function(){
                $(this).removeClass("selected")
                $(this).removeClass("table-info")
            })
            $(this).addClass("selected")
            $(this).addClass("table-info")
        }
    });

    $("#HouseholdExpenditureChange").click(function(){
        var value = parseInt($("#HouseholdExpenditureValue").val());
        if(!isNormalInteger(value) && value <= 0)
        {
            SetInvalid($("#HouseholdExpenditureValue"), $("#HouseholdExpenditureValueError"), "Enter positive integer");
            return;
        }
        post("/Game/ChangeHouseHoldExpenditure", {value: value});
    });
});