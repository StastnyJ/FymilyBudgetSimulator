$(function(){
    $('#navbar_top_banking').addClass("active");

    var table = $('#historyTable').DataTable({
        paging: false,
        scrollY: "60vh",
        scrollCollapse: true,   
        info: false,
        dom: "t"
    });

    $('#dateFrom').datetimepicker({
        format: "DD-MM-YYYY",
    });

    $("#dateFrom").val($("#dateFrom").attr("value"))

    $('#dateTo').datetimepicker({
        format: "DD-MM-YYYY",
    });

    $("#dateTo").val($("#dateTo").attr("value"))
});