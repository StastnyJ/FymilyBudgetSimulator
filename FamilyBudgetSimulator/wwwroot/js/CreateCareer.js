$(function(){
    $("#monthDurationSelect").change(function() {
        if($(this).val() === "-1"){
             enable($("#monthDuration"));
        }
        else {
            disable($("#monthDuration"));
            $("#monthDuration").val($(this).val());
        }    
    });

    $("#CreateCareerConfirm").click(function(){
        var careerId = $("#careerSelect").val();
        var monthDuration = $("#monthDuration").val();
        if(isNull(monthDuration) || monthDuration <= 0 || !isNormalInteger(monthDuration))
            SetInvalid($("#monthDuration"), $("#monthDurationError"), "You must enter positive number");
        else 
            post("/Home/CreateCareer/",{CareerTypeId: careerId, virtualMonthDuration: monthDuration});
    });
});