$('a[href="#Login"]').on('click', function (e) {
	e.preventDefault()
	$(this).tab('show')
})

//region registration

// #region validation

$('#RegisterUserName').focusout(function(){
	var validationToken = new RegExp('[a-zA-Z0-9_.*$€]{4,32}');
	var element = $(this);
	var test = validationToken.test(element.val());
	if(!test)
	{
		SetInvalid(element, $('#RegisterUserNameError'), "The name can contain just letters A-Z, digits and _.*$€ and it's length must be between 4 and 32 letters");
		return;
	}
	$.post("/Home/CheckUserNameUnique", {login: element.val()}).done(function(result){
		if(!result)
		{
			SetInvalid(element, $('#RegisterUserNameError'), "This user name already exists");
		}
		else
		{
			SetValid(element);
		}
	});
	
});

$('#RegisterEmail').focusout(function(){
	var validationToken = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	var element = $(this);
	if(!validationToken.test(element.val().toLowerCase()))
	{
		SetInvalid(element, $("#RegisterEmailError"), "email is in incorrect format");
		return;
	}
	$.post("/Home/CheckEmailUnique", {email: element.val()}).done(function(result){
		if(!result)
		{
			SetInvalid(element, $("#RegisterEmailError"), "email already exists");
			alert("unique error");
		}
		else{
			SetValid(element);
		}
	});
});

$('#RegisterPassword').focusout(function(){
	var validationToken = new RegExp('[a-zA-Z0-9_.*$€]{8,32}');
	var element = $(this);
	if(!validationToken.test(element.val()))
	{
		SetInvalid(element, $("#RegisterPasswordError"), "Incorrect format: length must be 8-32 letters and there are allowed only a-z characters, digits and symbols _.*$€.");
		return;
	}
	SetValid(element);
});

$('#RegisterPasswordComfirm').focusout(function(){
	var element = $(this);
	var pwd = $("#RegisterPassword").val();
	if(pwd != element.val())
	{
		SetInvalid(element, $("#RegisterPasswordComfirmError"), "Passwords are different");
		return;
	}
	SetValid(element);
});

$('#RegisterPassword').keyup(function(){
	var score = CountPasswordStrength($(this).val());
	var metter = $("#StrengthMetter");
	metter.css("width", score + "%");
	metter.removeClass("bg-success");
	metter.removeClass("bg-warning");
	metter.removeClass("bg-danger");
	metter.addClass(score < 50 ? "bg-danger" : score < 80 ? "bg-warning" : "bg-success");
});

// endregion

$('#SignUp').click(function(){
	var button = $(this);
	var isOk = true;
	if(!$("#RegisterUserName").hasClass("is-valid")) isOk = false;
	if(!$("#RegisterEmail").hasClass("is-valid")) isOk = false;
	if(!$("#RegisterPassword").hasClass("is-valid")) isOk = false;
	if(!$("#RegisterPasswordComfirm").hasClass("is-valid")) isOk = false;
	if(!isOk)
	{
		alert("invalid"); //TODO -> popup
		return;
	}
	LoadingButton(button);
	$.post("/Home/PostComfrimationEmail", {email : $("#RegisterEmail").val()}).done(function(result){
		alert(result); //TODO -> remove after deletation
		$("#FinishRegistrationUserName").val($("#RegisterUserName").val());
		$("#FinishRegistrationEmail").val($("#RegisterEmail").val());
		$("#FinishRegistrationModal").modal("show");
		ResetButton(button);
	});
});

$('#FinishRegistration').click(function(){
	var btn = $(this);
	LoadingButton(btn);
	var code = $("#EmailComfrimationToken").val();
	var email = $("#RegisterEmail").val();
	if(code === null || code === "")
	{
		alert("invalid"); //TODO -> popup
		ResetButton(btn)
		return;
	}
	$.post("/Home/CheckToken", {token: code, email: email}).done(function(result){
		if(result)
		{
			var name = $("#RegisterUserName").val();
			var pwd = $("#RegisterPassword").val();
			var type = $("#PremiumAccount").hasClass('selected') ? "Premium" : "Free";
			post("/Home/Register/", {userName: name, password: pwd, email: email, token: code, accountType: type});
		}
		else
		{
			alert("invalid"); //TODO -> popup
			ResetButton(btn);
			return;
		}
	})

});

$('#FreeAcount').click(function(){
	if(!$(this).hasClass("selected")) SelectAccountType("#FreeAcount", "#PremiumAccount");
});

$("#PremiumAccount").click(function(){
	if(!$(this).hasClass("selected")) SelectAccountType("#PremiumAccount", "#FreeAcount");
});

function SelectAccountType(selectName, deselectName)
{
	$(selectName).addClass("selected");
	$(deselectName).removeClass("selected");
	$(selectName + "Header").removeClass("bg-info");
	$(selectName + "Header").addClass("bg-primary");
	$(deselectName + "Header").removeClass("bg-primary");
	$(deselectName + "Header").addClass("bg-info");
	$(selectName + "Check").removeAttr("hidden", "hidden");
	$(deselectName + "Check").attr("hidden", "hidden");
}
//endregion

$("#btnLogIn").click(function(){
	LoadingButton($(this));
	var name = $("#LogInUserName").val();
	var pwd = $("#LogInPwd").val();
	var remember = $("#LogInRemember").is(':checked');
	if(name == null || name == "" || pwd == null || pwd == "")
	{
		alert("invalid"); //TODO -> popup
		ResetButton($(this));
		return;
	}
	post("/Home/Login", { name: name, password: pwd, remember: remember });
});