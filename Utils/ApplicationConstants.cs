namespace Utils
{
    public static class ApplicationConstants
    {
        public const string LOGGED_IN_SESSION_KEY = "LoggedIn";
        public const string LOGGED_ROLE_SESSION_KEY = "LoggedInRole";
        public const string USER_COOKIE_KEY = "PermanentUserId";
        public const string SAVING_DATETTIME_AS_STRING_FORMAT = "yyyyMMddHHmmssfffffff";
        public const string SYSTEM_ACCOUNT_NUMBER = "153-1824190644/0600";
    }
}