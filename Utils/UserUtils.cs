using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Linq;

namespace Utils
{
    public static class UserUtils
    {
        public static string HashPassword(string password)
        {           
            using(SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(password));
                return string.Join("", hash.Select(b => b.ToString("x2")).ToArray());
            }
        }
        public static string HashPassword(string password, string salt)
        {
            return HashPassword(SaltPassword(password, salt));
        }
        public static bool CheckPassword(string password, string expectedHash)
        {
            string hash = HashPassword(password);
            return hash == expectedHash;
        }
        public static bool CheckPassword(string password, string salt, string expectedHash)
        {
            return CheckPassword(SaltPassword(password, salt), expectedHash);
        }

        private static string SaltPassword(string password, string salt)
        {
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < password.Length; i++)
            {
                sb.Append(password[i] + salt[i % salt.Length]);
            }
            return sb.ToString();
        } 

        /// <summary>
        /// User name rules:
        ///     - length - <4;32>
        ///     - content - a-zA-Z0-9_.*$€
        /// </summary>
        /// <param name="UserName">Name to validate</param>
        /// <returns>Check the correction of the format</returns>
        public static bool VerifyUserNameFormat(string UserName)
        {
            return Regex.IsMatch(UserName, @"^[a-zA-Z0-9_.*$€]{4,32}$");
        }

        public static bool VerifyEmailFormat(string email)
        {
            try 
            {
                MailAddress mail = new MailAddress(email);
                return mail.Address == email;
            }
            catch 
            {
                return false;
            }
        }

        public static bool VerifyPasswordFormat(string password)
        {
            return Regex.IsMatch(password, @"^[a-zA-Z0-9_.*$€]{8,32}$");
        }

        public static bool VerifyUserAttributes(string login="aaaa", string email="aaaa@gmail.com", string password = "aaaaaaaa")
        {
            return VerifyUserNameFormat(login) && VerifyEmailFormat(email) && VerifyPasswordFormat(password);
        }

        public static string GenerateEmailToken(string email, int length = 8)
        {               
            using(SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(email));
                return string.Join("", hash.Select(b => b.ToString("x2")).ToArray()).Remove(length);
            }
        }

        public static bool CheckEmailToken(string token, string email, int length = 8)
        {
            return token == GenerateEmailToken(email, length);
        }

        public static int GetStartHappiness()
        {
            return 100;
        }

        public static int GetStartAge()
        {
            return 20;
        }

        public static DateTime CountVirtualDate(DateTime creationTime, double dayDuration)
        {
            return CountVirtualDate(creationTime, DateTime.Now, dayDuration);
        }

        public static DateTime CountVirtualDate(DateTime creationTime, DateTime actualTime, double dayDuration)
        {
            int minsDifference = (int)actualTime.Subtract(creationTime).TotalMinutes;
            int virtualDayDifference = (int)(minsDifference / dayDuration);   
            return creationTime.AddDays(virtualDayDifference);   
        }

        public static DateTime TransferVirtualToActualDate(DateTime creationTime, DateTime virtualTime, double dayDuration)
        {
            int virtualDaysDiff = (int)virtualTime.Subtract(creationTime).TotalDays;
            int actualMinsDiff = (int)(virtualDaysDiff * dayDuration);
            return creationTime.AddMinutes(actualMinsDiff);
        }

        public static double GetUserAge(DateTime creationTime, double virtualDayDuration){
            DateTime virtualTimeNow = CountVirtualDate(creationTime, virtualDayDuration);
            return UserUtils.GetStartAge() + BasicUtils.DaysToYears(virtualTimeNow.Subtract(creationTime).TotalDays);
        }
    }
}