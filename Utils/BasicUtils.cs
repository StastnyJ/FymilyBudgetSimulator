using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Linq;

namespace Utils
{
    public static class BasicUtils
    {
        private static Random rnd = new Random();
       
        public static int GenRandomNumber(int min = int.MinValue, int max = int.MaxValue)
        {
            return rnd.Next(min, max);
        }

        public static double GenRandomDouble(double min = double.MinValue, double max = double.MaxValue)
        {
            return rnd.NextDouble() * (max - min) + min;
        }

        public static bool IsBetween(this int val, int first, int sec)
        {
            return val >= first && val < sec;
        }

        public static bool IsBetween(this double val, double first, double sec){
            return val >= first && val <= sec;
        }

        public static double DaysToYears(double days)
        {
            return days * 0.00273790926;
        }

        public static double YearsToDays(double years)
        {
            return years * 365.242199;
        }
        public static DateTime GetNextDayOfWeek(DateTime act, DayOfWeek day)
        {
            while(true)
            {
                act = act.AddDays(1);
                if(act.DayOfWeek == day) return act;
            }
        }

        public static DateTime GetStartOfNextMonth(this DateTime act)
        {
            DateTime res = act.AddMonths(1);
            res = res.AddDays(1 - res.Day);
            return res;
        }

    }
}