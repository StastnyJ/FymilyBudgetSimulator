using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Linq;

namespace Utils
{
    public static class BankingUtils
    {
       public static string GenerateBankAccount(DateTime time)
       {
           string year = time.ToString("yyyy");
           string month = time.ToString("MM");
           string day = time.ToString("dd");
           string hours = time.ToString("HH");
           string mins = time.ToString("mm");
           string secs = time.ToString("ss");
           string milsecs = time.Millisecond.ToString("0000").Remove(2);
           return string.Join("",year[0], milsecs[0], year[1], "-", year.Remove(0,2), day, hours, month, secs, "/", "0", milsecs[1], "00");
       }



        public static int GetStarterBalance()
        {
            return 1000;
        }
    }
}