using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Linq;

namespace Utils
{
    public static class EconomyUtils
    {
        private static readonly DateTime ECONOMY_CYCLE_START_DATE = new DateTime(2018,1,1);
        public static int GetStartHouseholdExpenditure()
        {
           return 50;
        }
                
        public static Tuple<int, double, DateTime, double> GetActualCycleInformation(DateTime act)
        {
            double daysDiff = (act - ECONOMY_CYCLE_START_DATE).TotalDays;
            double yearsDiff = BasicUtils.DaysToYears(daysDiff);
            double durationSum = 0;
            for(int i = 0; true; i++)
            {
                double duration = getCycleDuration(i);
                yearsDiff -= duration;
                if(yearsDiff < 0)
                {
                    int cycleNumber = i;
                    DateTime startDate = ECONOMY_CYCLE_START_DATE.AddDays(BasicUtils.YearsToDays(durationSum)).Date;
                    double yearsFromStart = BasicUtils.DaysToYears((act - startDate).TotalDays);
                    double partOfCycle = yearsFromStart / duration;
                    return new Tuple<int, double, DateTime, double>(cycleNumber, duration, startDate, partOfCycle);
                }
                durationSum += duration;
            }
        } 
        
        public static int GetActualCycleNumber(DateTime act)
        {
            return GetActualCycleInformation(act).Item1;   
        }

        public static double GetActualCycleDuration(DateTime act)
        {
            return GetActualCycleInformation(act).Item2; 
        }

        public static DateTime GetActualCycleSatrt(DateTime act)
        {
            return GetActualCycleInformation(act).Item3;
        }

        public static double GetActualCyclePosition(DateTime act)
        {
            return GetActualCycleInformation(act).Item4;
        }

        private static double getCycleDuration(int cycleCount, double min = 2, double max = 4)
        {
            return min + ((double)(Math.Abs(Math.Pow(13, cycleCount) + 1253431 * Math.Pow(cycleCount, 17) + 847913 * Math.Pow(cycleCount, 7) + 323961313) % ((max - min) * 100000))) / 100000.0;
        }

        public static double CountLifeChance(double years)
        {
            return 0.000204 - 0.00000572 * years + 0.0000026 * years * years - 0.000000114 * years * years * years + 0.00000000459 * years * years * years * years;
        }
        
    }
}