using System;
using Xunit;
using Utils;
using System.Collections.Generic;
using System.Linq;

namespace UnitTests
{
    static class helpFuncs
    {
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            return new string(Enumerable.Repeat(new List<char>(), length).Select(s => (char)(random.Next(20, 128))).ToArray());
        }
        public static int RandomNumber(int min, int max)
        {
            return random.Next(min, max);
        }
    }
    public class UtilitiesTests
    {
        [Fact]
        public void PasswordHashing()
        {
            for(int i = 0; i < 10; i++)
            {
                string password =  helpFuncs.RandomString(helpFuncs.RandomNumber(8, 32));
                string salt = helpFuncs.RandomString(helpFuncs.RandomNumber(8, 32));
                
                string hash = UserUtils.HashPassword(password);
                string hashSalt = UserUtils.HashPassword(password, salt);

                Assert.True(UserUtils.CheckPassword(password, hash));
                Assert.True(UserUtils.CheckPassword(password, salt, hashSalt));
            }
        }
    }
}