using System;
using DatabaseModels;
using Utils;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace DatabaseRepositories
{
    public class ActionRepository
    {
        private readonly FBSDbContext FBSDb;
        public ActionRepository(FBSDbContext context)
        {
            FBSDb = context;
        }

        public int GetNewId()
        {
            int? id = FBSDb.WaitingActions.OrderByDescending(a => a.Id).Select(a => a.Id).FirstOrDefault();
            if(id == null) id = -1;
            return (int)id + 1;
        }

        public bool CreateAction(ScheduleAction action, bool setId = true, bool saveChanges = true)
        {
            if(setId) action.Id = GetNewId();
            FBSDb.WaitingActions.Add(action);
            if(saveChanges) return FBSDb.TrySaveChanges();
            return true;
        }

        public bool CreateActions(List<ScheduleAction> actions, bool setId = true, bool saveChanges = true)
        {
            int id = 0;
            if(setId) id = GetNewId();
            foreach(var action in actions)
            {
                if(setId) action.Id = id++;
                CreateAction(action, false, false);
            }
            if(saveChanges) return FBSDb.TrySaveChanges();
            return true;
        }

        public bool DeleteAction(ScheduleAction action, bool saveChanges = true)
        {
            FBSDb.WaitingActions.Remove(action);
            if(saveChanges) return FBSDb.TrySaveChanges();
            return true;
        }

        public bool CreateSavingsActions(SavingsAccount account, User usr, bool saveChanges = true)
        {
            DateTime executionDate = DateTime.Now;
            DateTime virtualDate = UserUtils.CountVirtualDate(usr.TimeOfCreation, usr.Properties.VirtualDayDuration);
            List<ScheduleAction> actions = new List<ScheduleAction>();
            for(int i = 0; i < account.Duration; i++)
            {
                int daysDiff = (int)virtualDate.AddMonths(1).Subtract(virtualDate).TotalDays;
                virtualDate = virtualDate.AddMonths(1);
                executionDate = executionDate.AddMinutes((int)(daysDiff * usr.Properties.VirtualDayDuration));
                PaySavingsAccountAction action = new PaySavingsAccountAction(){
                    ActionDate = executionDate,
                    UserId = usr.Id,
                    TimeOfSavingsAccountCreation = account.CreationTime,
                    CollectMoney = i == account.Duration - 1
                };
                actions.Add(action);
            }
            return CreateActions(actions, true, saveChanges);
        }

        public bool CreatePayHouseholdHexpeditureAction(User usr, bool saveChanges = true)
        {
            DateTime virtualDateOfPayment = BasicUtils.GetNextDayOfWeek(UserUtils.CountVirtualDate(usr.TimeOfCreation, usr.Properties.VirtualDayDuration), DayOfWeek.Monday);
            HouseholdExpenditureAction action = new HouseholdExpenditureAction(){
                ActionDate = UserUtils.TransferVirtualToActualDate(usr.TimeOfCreation, virtualDateOfPayment, usr.Properties.VirtualDayDuration),
                UserId = usr.Id
            };
            return CreateAction(action, saveChanges: saveChanges);
        }

        public bool CreateInvestitionFondInterestAction(Fond fond, User usr, bool saveChanges = true)
        {
            DateTime virtualDate = UserUtils.CountVirtualDate(usr.TimeOfCreation, usr.Properties.VirtualDayDuration);
            DateTime nextVirtualInterest = virtualDate.GetStartOfNextMonth();
            InvestitionFondInterestAction action = new InvestitionFondInterestAction(){
                ActionDate = UserUtils.TransferVirtualToActualDate(usr.TimeOfCreation, nextVirtualInterest, usr.Properties.VirtualDayDuration),
                UserId = usr.Id,
                TimeOfFondCreation = fond.CreationTime
            };
            return CreateAction(action, saveChanges: saveChanges);
        }

        public bool DeleteInvestitionFondInterestAction(Fond fond, bool saveChanges = true)
        {
            ScheduleAction action = getInvestitionFondInterestAction(fond);
            if(action == null) return false;
            FBSDb.WaitingActions.Remove(action);
            if(saveChanges) return FBSDb.TrySaveChanges();
            return true;
        }

        public ScheduleAction getInvestitionFondInterestAction(Fond fond)
        {
            string dateString = fond.CreationTime.ToString("yyyyMMddHHmmssfffffff");
            return FBSDb.WaitingActions.Where(a => a.ActionTypeId == ScheduleActionType.InvestitionFondInterest && a.Data.StartsWith(dateString) && a.UserId == fond.Account.UserProperties.UserId).SingleOrDefault();
        }

        public IEnumerable<ScheduleAction> getSavingsActions(SavingsAccount account)
        {
            string dateString = account.CreationTime.ToString("yyyyMMddHHmmssfffffff");
            return FBSDb.WaitingActions.Where(a => a.ActionTypeId == ScheduleActionType.PaySavingsAccountAction && a.Data.StartsWith(dateString) && a.UserId == account.Account.UserProperties.UserId);
        }

        public bool RemoveSavingsActions(SavingsAccount account, bool saveChanges = true)
        {
            var actions = getSavingsActions(account);
            FBSDb.WaitingActions.RemoveRange(actions);
            if(saveChanges) return FBSDb.TrySaveChanges();
            return true; 
        }

        public bool CreateInsuranceAction(User usr, Insurance insurance, bool saveChanges = true)
        {
            DateTime virtualDate = UserUtils.CountVirtualDate(usr.TimeOfCreation, usr.Properties.VirtualDayDuration);
            DateTime nextVirtualPayment = virtualDate.GetStartOfNextMonth();
            PayInsuranceAction action = new PayInsuranceAction(){
                ActionDate = UserUtils.TransferVirtualToActualDate(usr.TimeOfCreation, nextVirtualPayment, usr.Properties.VirtualDayDuration),
                Insurance = insurance,
                UserId = usr.Id
            };
            return CreateAction(action, true, saveChanges);
        }

        public IEnumerable<ScheduleAction> getWaitingActions()
        {
            return getWaitingActions(DateTime.Now);
        }

        public IEnumerable<ScheduleAction> getWaitingActions(DateTime toDate)
        {
            return FBSDb.WaitingActions.Where(a => a.ActionDate <= toDate).OrderBy(a => a.ActionDate);
        }

    }
}