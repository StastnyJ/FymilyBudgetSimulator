﻿using System;
using DatabaseModels;
using Utils;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace DatabaseRepositories
{    public class UserRepository
    {
        private readonly FBSDbContext FBSDb;
        public UserRepository(FBSDbContext context)
        {
            FBSDb = context;
        }

        public bool TestUserUnique(string login)
        {
            User user = GetUser(login);
            return user == null;
        }

        public bool TestEmailUnique(string email)
        {
            User user = GetUsers(filter: u => u.Email == email).FirstOrDefault();
            return user == null;
        }
        
        public bool TestEmailAndLoginUnique(string login, string email)
        {
            User user = GetUsers(filter: u => u.Email == email && u.Login == login).FirstOrDefault();
            return user == null;
        }

        public int GenerateNewId()
        {
            var last = GetAllUsers().OrderByDescending(u => u.Id).FirstOrDefault();
            if(last == null) return 1;
            return last.Id + 1;
        }

        public User CreateUser(User user)
        {
            try
            {
                FBSDb.Add<User>(user);
                FBSDb.SaveChanges();
                return user;
            }
            catch
            {
                return null;
            }
        }        

        public User CreateUser(string name, string email, string password, int roleId = 0)
        {
            User usr = new User()
            {
                Login = name,
                Email = email,
                PasswordHash = UserUtils.HashPassword(password, name),
                RoleId = roleId,
                Id = GenerateNewId(),
                TimeOfCreation = DateTime.Now,
                TimeOfLastLogIn = DateTime.Now
            };
            return CreateUser(usr);
        }

        public User TestLoginPassword(string login, string password)
        {
            User user = GetUser(login);
            if(user == null) return null;
            if(!UserUtils.CheckPassword(password, login, user.PasswordHash)) return null;
            return user;
        }

        public IEnumerable<User> GetAllUsers()
        {
            return GetUsers();
        }

        public IEnumerable<User> GetUsers(Func<User, bool> filter = null)
        {
            if(filter == null) filter = a => true;
            var selection = FBSDb.Users.Where(filter);
            return selection;
        }

        public User GetUser(string login)
        {
            return FBSDb.Users.Where(u => u.Login == login).FirstOrDefault();
        }

        public User GetUser(int id)
        {
            return FBSDb.Users.Where(u => u.Id == id).FirstOrDefault();
        }

        public void UpdateLastLogin(int id)
        {
            UpdateLastLogin(GetUser(id));
        }

        public void UpdateLastLogin(User usr)
        {
            usr.TimeOfLastLogIn = DateTime.Now;
            FBSDb.Entry<User>(usr).State = EntityState.Modified;
            FBSDb.SaveChanges();
        }       

        public bool CreateUserProperties(UserProperties prop, BankingRepository rep)
        {
            FBSDb.UsersProperties.Add(prop);
            return rep.CreateBankingAccount(prop.AccountNumber);
        }

        public bool CreateUserProperties(int UserId, int CareerId, double virtualDayDuration, BankingRepository rep)
        {
            UserProperties prop = new UserProperties(){
                AccountNumber = BankingUtils.GenerateBankAccount(DateTime.Now),
                EquipmentId = -1,
                FirstCarId = -1,
                SecondCarId = -1,
                Happiness = UserUtils.GetStartHappiness(),
                HouseholdExpenditure = EconomyUtils.GetStartHouseholdExpenditure(),
                HouseId = -1,
                JobId = -1,
                VirtualDayDuration = virtualDayDuration,
                UserId = UserId  
            };
            return CreateUserProperties(prop, rep);
        }

        public bool CreateUserProperties(User usr, int CareerId, double virtualDayDuration, BankingRepository rep)
        {
            return CreateUserProperties(usr.Id, CareerId, virtualDayDuration, rep);
        }
    }
}
