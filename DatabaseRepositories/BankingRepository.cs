using System;
using DatabaseModels;
using Utils;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace DatabaseRepositories
{    public class BankingRepository
    {
        private readonly FBSDbContext FBSDb;
        public BankingRepository(FBSDbContext context)
        {
            FBSDb = context;
        }

        public bool CreateBankingAccount(Account account)
        {
            FBSDb.Accounts.Add(account);
            return FBSDb.TrySaveChanges();
        }

        public bool CreateBankingAccount(string accountNumber)
        {
            Account act = new Account(){
                AccountNumber = accountNumber,
                Balance = BankingUtils.GetStarterBalance()
            };
            return CreateBankingAccount(act);
        }

        public Tuple<DateTime, double>[] GetNearestHistory(Account accout){
            return accout.History.OrderByDescending(h => h.Date).Select(h => new Tuple<DateTime, double>(h.Date, h.Change)).Take(3).ToArray();
        }

        public bool CheckAccountExistence(string account)
        {
            return FBSDb.Accounts.Where(a => a.AccountNumber == account).SingleOrDefault() != null;
        }

        public Account GetAccount(string accountNumber)
        {
            return FBSDb.Accounts.Where(a => a.AccountNumber == accountNumber).SingleOrDefault();
        }

        public Account UpdateBalance(Account account, double value, bool saveChanges = true)
        {
            if(account.Balance + value < 0)
                return account;
                
            account.Balance += value;
            FBSDb.Entry<Account>(account).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            if(saveChanges) FBSDb.SaveChanges();
            return account;
        }

        public bool UpdateBalanceAndSaveHistory(Account account, double value, string counterfactNumber, int vs, string message, bool saveChanges = true)
        {
            UpdateBalance(account, value, false);
            SaveHistory(account, counterfactNumber, account.Balance - value, vs, message, false);
            if(saveChanges) return FBSDb.TrySaveChanges();
            return true;
        }
        public Account UpdateBalance(string accountNumber, int value, bool saveChanges = true)
        {
            return UpdateBalance(GetAccount(accountNumber), value, saveChanges);
        }

        public TransactionHistory SaveHistory(Account account, string counterfactNumber,  double oldValue, int vs, string message, bool saveChanges = true)
        {
            return SaveHistory(account.AccountNumber, counterfactNumber, oldValue, account.Balance, vs, message, saveChanges);
        }

        public TransactionHistory SaveHistory(string accountNumber, string counterfactNumber, double oldValue, double newValue, int vs, string message, bool saveChanges = true)
        {
            TransactionHistory his = new TransactionHistory(){
                AccountNumber = accountNumber,
                CounterfactNumber = counterfactNumber,
                Date = DateTime.Now,
                OldBalance = oldValue,
                NewBalance = newValue,
                Message = message,
                VariableSymbol = vs
            };
            FBSDb.TransactionHistory.Add(his);
            if(saveChanges) FBSDb.SaveChanges();
            return his;
        }

        public List<TransactionHistory> GetHistory(Account account, DateTime dateFrom, DateTime dateTo)
        {
            return FBSDb.TransactionHistory.Where(h => h.AccountNumber == account.AccountNumber && h.Date >= dateFrom && h.Date < dateTo).OrderByDescending(h => h.Date).ToList();
        }

        public SavingsAccountType GetSavingsAccountType(int id)
        {
            return FBSDb.SavingsAccountTypes.Where(a => a.Id == id).SingleOrDefault();
        }

        public bool CreateSavingsAccount(User usr, int savingsType, int monthlyFee, int duration, ActionRepository actionRep, bool save=true)
        {
            SavingsAccount account = new SavingsAccount()
            {
                AccountId = usr.Properties.AccountNumber,
                AccountTypeId = savingsType,
                CreationTime = DateTime.Now,
                Duration = duration,
                MonthlyFee = monthlyFee
            };
            FBSDb.SavingsAccounts.Add(account);
            return actionRep.CreateSavingsActions(account, usr, save);
        }

        public SavingsAccount GetSavingsAccount(string accountNumber, DateTime date)
        {
            return FBSDb.SavingsAccounts.Where(a => a.AccountId == accountNumber && a.CreationTime == date).SingleOrDefault();
        }

        public SavingsAccount GetSavingsAccount(string accountNumber, string dateString)
        {
            DateTime date = DateTime.ParseExact(dateString, ApplicationConstants.SAVING_DATETTIME_AS_STRING_FORMAT, null);
            return GetSavingsAccount(accountNumber, date);
        }

        public SavingsAccount UpdateSavingsAccountMoney(SavingsAccount account, int value, bool saveChanges = true)
        {
            account.InvestedMoney += value;
            FBSDb.Entry<SavingsAccount>(account).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            if(saveChanges) FBSDb.SaveChanges();
            return account;
        }

        public bool WithdrawSavingsAccount(SavingsAccount account, ActionRepository actionRep, bool saveChanges = true)
        {
            if(account.Account.Balance + account.InvestedMoney < account.AccountType.WithdrawalFee) return false;
            account.InvestedMoney -= account.AccountType.WithdrawalFee;
            UpdateBalance(account.Account, account.InvestedMoney, false);
            SaveHistory(account.Account, ApplicationConstants.SYSTEM_ACCOUNT_NUMBER, account.Account.Balance - account.InvestedMoney, 1, string.Format("{0} account withdrawal", account.AccountType.Description), false);
            actionRep.RemoveSavingsActions(account, false);
            return RemoveSavingsAccount(account, saveChanges);
        }

        public bool RemoveSavingsAccount(SavingsAccount account, bool saveChanges = true)
        {
            FBSDb.SavingsAccounts.Remove(account);
            if(saveChanges) return FBSDb.TrySaveChanges();
            return true;
        }

        public bool PaySavingsAccount(Account account, SavingsAccount savingsAccount, bool saveChanges = true)
        {
            if(account.Balance < savingsAccount.MonthlyFee)
                return false;
            UpdateBalance(account, -1 * savingsAccount.MonthlyFee, false);
            UpdateSavingsAccountMoney(savingsAccount, savingsAccount.MonthlyFee, false);
            SaveHistory(account, ApplicationConstants.SYSTEM_ACCOUNT_NUMBER, account.Balance + savingsAccount.MonthlyFee, 1, string.Format("Schedule payment to your {0}", savingsAccount.AccountType.Description), false);
            if(saveChanges) return FBSDb.TrySaveChanges();
            return true;
        }

        public FondType GetFondType(int id)
        {
            return FBSDb.FondTypes.Where(f => f.Id == id).SingleOrDefault();
        }

        public bool CreateFond(int fondType, int investedMoney, Account account, ActionRepository actionRep, bool saveChanges = true)
        {
            if(account.Balance < investedMoney) return false;
            UpdateBalanceAndSaveHistory(account, -investedMoney, ApplicationConstants.SYSTEM_ACCOUNT_NUMBER, 1, "Investition fond creation", false);
            Fond fond = new Fond(){
                AccountNumber = account.AccountNumber,
                ActualMoney = investedMoney,
                CreationTime = DateTime.Now,
                FondTypeId = fondType,
                InvestedMoney = investedMoney
            };
            FBSDb.Fonds.Add(fond);
            return actionRep.CreateInvestitionFondInterestAction(fond, account.UserProperties.User, saveChanges);
        }

        public Fond GetFond(Account account, string creationTimeString)
        {
            DateTime creationTime = DateTime.ParseExact(creationTimeString ,"yyyyMMddHHmmssfffffff", null);
            return GetFond(account, creationTime);
        }

        public Fond GetFond(Account account, DateTime creationTime)
        {
            return account.Fonds.Where(f => f.CreationTime == creationTime).SingleOrDefault();
        }
        
        public bool CollectFromFond(Account account, string creationTime, ActionRepository actionRep, bool saveChanges = true)
        {
            Fond fond = GetFond(account, creationTime);
            if(fond == null) return false;
            UpdateBalanceAndSaveHistory(account, fond.ActualMoney * 0.995, ApplicationConstants.SYSTEM_ACCOUNT_NUMBER, 1, "Collecting from investition fond", false);
            FBSDb.Remove(fond);
            return actionRep.DeleteInvestitionFondInterestAction(fond, saveChanges);
        }

        public bool InterestFond(Fond fond, bool saveChanges = true)
        {
            DateTime virtualDate = UserUtils.CountVirtualDate(fond.Account.UserProperties.User.TimeOfCreation, fond.Account.UserProperties.VirtualDayDuration);
            double interest = fond.FondType.GenerateInterest(virtualDate);
            fond.ActualMoney *= 1 + interest;
            FBSDb.Entry<Fond>(fond).State = EntityState.Modified;
            if(saveChanges) return FBSDb.TrySaveChanges();
            return true;
        }

        public bool ValidateFondType(int fondType)
        {
            return GetFondType(fondType) != null;
        }
        public bool ChangeHouseholdExpenditure(User usr, int newExpenditure, bool saveChanges = true)
        {
            if(newExpenditure <= 0) return false;
            if(usr.Properties.HouseholdExpenditure == newExpenditure) return true;
            usr.Properties.HouseholdExpenditure = newExpenditure;
            FBSDb.Entry<UserProperties>(usr.Properties).State = EntityState.Modified;
            if(saveChanges) return FBSDb.TrySaveChanges();
            return true;
        }

        public bool CreateInsurance(Insurance insurance, bool saveChanges = true)
        {
            FBSDb.Insurances.Add(insurance);
            if(saveChanges) return FBSDb.TrySaveChanges();
            return true;
        }

        public bool CreateInvalidityInsurance(User usr, double insuredMoney, int type, ActionRepository actionRep, bool saveChanges = true){
            InvalidityInsurance insurance = new InvalidityInsurance(){
                AccountNumber = usr.Properties.AccountNumber,
                InsuredMoney = insuredMoney,
                Type = (InvalidityInsurance.Types)type
            };
            UpdateBalanceAndSaveHistory(usr.Properties.Account, -InvalidityInsurance.CreationFee, ApplicationConstants.SYSTEM_ACCOUNT_NUMBER, 0, "Creational fee for invalidity investition", false);
            CreateInsurance(insurance, false);
            return actionRep.CreateInsuranceAction(usr, insurance, saveChanges);
        }
    }
}
