using System;
using DatabaseModels;
using Utils;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace DatabaseRepositories
{    public class CareerRepository
    {
        private readonly FBSDbContext FBSDb;
        public CareerRepository(FBSDbContext context)
        {
            FBSDb = context;
        }

        public bool CreateCareer(User usr, int careerType)
        {
            return CreateCareer(usr.Id, careerType);
        }

        public int GetCareerId()
        {
            int? id = FBSDb.Careers.OrderByDescending(a => a.Id).Select(a => a.Id).FirstOrDefault();
            return id == null ? 0 : (int)id + 1;
        }

        public bool CreateCareer(Career career)
        {
            FBSDb.Careers.Add(career);
            return FBSDb.TrySaveChanges();
        }

        public bool CreateCareer(int userId, int careerType)
        {
            if(!checkType(careerType)) return false;
            Career act = new Career(){
                CareerTypeId = careerType,
                CreatedTime = DateTime.Now,
                Id = GetCareerId(),
                UserId = userId
            };
            return CreateCareer(act);
        }
        private bool checkType(int careerType)
        {
            return CareerTypes.All.Contains(careerType);
        }
    }
}
