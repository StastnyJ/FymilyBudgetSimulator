using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace DatabaseModels
{
    [Table("Accounts")]
    public class Account
    {  
        [Key]
        [Column("Account")]
        public string AccountNumber {get; set;}
        
        [Column("Balance")]
        public double Balance {get; set;}

        [NotMapped]
        public double ViewBalance{
            get{
                return Math.Round(Balance, 2);
            }
        }

        public virtual UserProperties UserProperties{get;set;}
        public virtual List<TransactionHistory> History{get; set;}
        public virtual List<SavingsAccount> SavingsAccounts {get;set;}
        public virtual List<Fond> Fonds {get;set;}
        public virtual List<Insurance> Insurances{get;set;}

        [NotMapped]
        public InvalidityInsurance InvalidityInsurance{
            get{
                Insurance insurance = Insurances.Where(i => i.InsuranceType == InsuranceTypes.INVALIDITY_INSURANCE).SingleOrDefault();
                if(insurance == null) return null;
                return new InvalidityInsurance(insurance);
            }
        }
    }   
}