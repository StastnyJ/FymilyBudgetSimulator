using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using Utils;

namespace DatabaseModels
{
    [Table("Insurances")]
    public class Insurance{

        public Insurance(){}

        public Insurance(Insurance insurance){
            CreationTime = insurance.CreationTime;
            AccountNumber = insurance.AccountNumber;
            InsuredMoney = insurance.InsuredMoney;
            Data = insurance.Data;
            DataDescription = insurance.DataDescription;
            Data = insurance.Data;
            DataDescription = insurance.DataDescription;
            InsuranceType = insurance.InsuranceType;
            Account = insurance.Account;
        }

        [Column("CreationTime")]
        public DateTime CreationTime {get;set;}

        [Column("Account")]
        public string AccountNumber {get;set;}

        [Column("InsuredMoney")]
        public double InsuredMoney {get;set;}
 

        [Column("Type")]
        public int InsuranceType {get;set;}

        [Column("Data")]
        public string Data {get; set;}

        [Column("DataDescription")]
        public string DataDescription {get;set;}

        [NotMapped]
        public static double CreationFee {
            get{
                return double.NaN;
            }
        }

        [NotMapped]
        public virtual double MonthlyFee{
            get{
                return double.NaN;
            }
        }

        [NotMapped]
        public static double MinimalInsuredMoney{
            get{
                return double.NaN;
            }
        }

        [NotMapped]
        public static double MaximalInsuredMoney{
            get{
                return double.NaN;
            }
        }

        [NotMapped]
        public static double InsuredMoneyStep{
            get{
                return double.NaN;
            }
        }

        [NotMapped]
        public Insurance SpecificInsurance{
            get{
                switch(InsuranceType){
                    case InsuranceTypes.LIFE_INSURANCE:
                        return new LifeInsurance(this);
                    case InsuranceTypes.INVALIDITY_INSURANCE:
                        return new InvalidityInsurance(this);
                    default:
                        return null;
                }
            }
        }

        public override string ToString()
        {
            return "Insurance";
        }

        public static bool ValidateInsuredMoney(double money){
            return money.IsBetween(MinimalInsuredMoney, MaximalInsuredMoney) && (money - MinimalInsuredMoney) % InsuredMoneyStep == 0;
        }

        [ForeignKey("AccountNumber")]
        public virtual Account Account {get;set;}
    }

    public class LifeInsurance : Insurance
    {
        public LifeInsurance(){
            Data = "";
            DataDescription = "";
            CreationTime = DateTime.Now;
            InsuranceType = InsuranceTypes.LIFE_INSURANCE;
        }

        public LifeInsurance(Insurance insurance) : base(insurance){}

        [NotMapped]
        public override double MonthlyFee{
            get{
                return CountMonthlyFee(InsuredMoney, 0 /*TODO - Motorgage*/, Account.UserProperties.User);
            }
        }

         [NotMapped]
        public new static double MinimalInsuredMoney{
            get{
                return 10000;
            }
        }

        [NotMapped]
        public new static double MaximalInsuredMoney{
            get{
                return 150000;
            }
        }

        [NotMapped]
        public new static double InsuredMoneyStep{
            get{
                return 1000;
            }
        }

        [NotMapped]
        public new static double CreationFee{
            get{
                return 5;
            }
        }

        public new static bool ValidateInsuredMoney(double money){
            return money.IsBetween(MinimalInsuredMoney, MaximalInsuredMoney) && (money - MinimalInsuredMoney) % InsuredMoneyStep == 0;
        }

        public static double CountMonthlyFee(double insuredMoney, double mortgage, double years)
        {
            return 0.08 + 2500 * EconomyUtils.CountLifeChance(years) * (0.00000000004 * insuredMoney * insuredMoney + 0.0001 * mortgage * mortgage);
        }

        public static double CountMonthlyFee(double insuredMoney, double mortgage, User usr)
        {
            return CountMonthlyFee(insuredMoney, mortgage, UserUtils.GetUserAge(usr.TimeOfCreation, usr.Properties.VirtualDayDuration));
        }

        public override string ToString(){
            return "Life Insurance";
        }
    }

    public class InvalidityInsurance : Insurance
    {
        public InvalidityInsurance()
        {
            DataDescription = "Insurance class";
            CreationTime = DateTime.Now;
            InsuranceType = InsuranceTypes.INVALIDITY_INSURANCE;
        }

        public InvalidityInsurance(Insurance insurance) : base(insurance){}
        
        [NotMapped]
        public Types Type{
            get{
                return Data == "1" ? Types.FirstDegree : Data == "2" ? Types.SecondDegree : Types.ThirdDegree;
            }
            set{
                Data = value == Types.FirstDegree ? "1" : value == Types.SecondDegree ? "2" : "3";
            }
        }

        public enum Types {
            FirstDegree = 1,
            SecondDegree = 2,
            ThirdDegree = 3
        }

        [NotMapped]
        public override double MonthlyFee{
            get{
                return CountMonthlyFee(InsuredMoney, Account.UserProperties.User, Type);
            }
        }

        [NotMapped]
        public new static double MinimalInsuredMoney{
            get{
                return 10000;
            }
        }

        [NotMapped]
        public new static double MaximalInsuredMoney{
            get{
                return 150000;
            }
        }

        [NotMapped]
        public new static double InsuredMoneyStep{
            get{
                return 1000;
            }
        }

        [NotMapped]
        public new static double CreationFee{
            get{
                return 5;
            }
        }

        public new static bool ValidateInsuredMoney(double money){
            return money.IsBetween(MinimalInsuredMoney, MaximalInsuredMoney) && (money - MinimalInsuredMoney) % InsuredMoneyStep == 0;
        }

        public static double CountMonthlyFee(double insuredMoney, double age, Types type){
            double secondPart = type == Types.ThirdDegree ? 15*age - 100 : type == Types.SecondDegree ? 25 * age - 100 : age * age - 40 * age + 1000;
            return (0.000000000006825 * insuredMoney * insuredMoney + 0.028) * secondPart;
        }

        public static double CountMonthlyFee(double insuredMoney, User usr, Types type){
            return CountMonthlyFee(insuredMoney, UserUtils.GetUserAge(usr.TimeOfCreation, usr.Properties.VirtualDayDuration), type);   
        }

        public override string ToString(){
            return "Invalidity Insurance";
        } 
    }

    public static class InsuranceTypes{
        public const int LIFE_INSURANCE = 0;
        public const int INVALIDITY_INSURANCE = 1;
    }
}