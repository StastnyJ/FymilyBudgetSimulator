using Microsoft.EntityFrameworkCore;

namespace DatabaseModels
{
    public class FBSDbContext : DbContext
    {
        public FBSDbContext(DbContextOptions<FBSDbContext> options):base(options){}
        
        public DbSet<User> Users {get; set; }
        public DbSet<Role> Roles {get; set; }
        public DbSet<Career> Careers {get; set; }
        public DbSet<UserProperties> UsersProperties {get; set;}
        public DbSet<Account> Accounts {get; set;}
        public DbSet<TransactionHistory> TransactionHistory {get;set;}
        public DbSet<SavingsAccount> SavingsAccounts {get;set;}
        public DbSet<SavingsAccountType> SavingsAccountTypes {get;set;}
        public DbSet<Fond> Fonds {get;set;}
        public DbSet<FondType> FondTypes {get;set;}
        public DbSet<Insurance> Insurances {get;set;}
        public DbSet<ScheduleAction> WaitingActions {get;set;}

        public bool TrySaveChanges(bool saveForAll = true)
        {
            try{
                this.SaveChanges(saveForAll);
                return true;
            }   
            catch{
                return false;
            }
        }

        protected override void OnModelCreating(ModelBuilder builder){
            builder.Entity<TransactionHistory>().HasKey(c => new {c.AccountNumber, c.Date});
            builder.Entity<SavingsAccount>().HasKey(c => new {c.AccountId, c.CreationTime});
            builder.Entity<Fond>().HasKey(c => new {c.CreationTime, c.AccountNumber});
            builder.Entity<Insurance>().HasKey(c => new{c.AccountNumber, c.InsuranceType});
        }
    }
}