using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using Utils;
using System.Linq;
using System.Collections.Generic;

namespace DatabaseModels
{
    [Table("Fonds")]
    public class Fond
    {
        [Column("CreationTime")]
        public DateTime CreationTime {get;set;}

        [Column("AccountNumber")]
        public string AccountNumber {get;set;}

        [Column("FondType")]
        public int FondTypeId {get;set;}

        [Column("InvestedMoney")]
        public double InvestedMoney {get;set;}

        [Column("ActualMoney")]
        public double ActualMoney {get;set;}

        [ForeignKey("AccountNumber")]
        public virtual Account Account {get;set;}

        [ForeignKey("FondTypeId")]
        public virtual FondType FondType {get;set;}
    }

    [Table("FondTypes")]
    public class FondType
    {
        [Key]
        [Column("Id")]
        public int Id {get;set;}

        [Column("Description")]
        public string Description {get;set;}

        [Column("MaximalInterest")]
        public double MaximalInterest {get;set;}

        [Column("I1Min")]
        public double I1Min {get;set;}

        [Column("I1Max")]
        public double I1Max {get;set;}

        [Column("I2Min")]
        public double I2Min {get;set;}

        [Column("I2Max")]
        public double I2Max {get;set;}

        [Column("I3Min")]
        public double I3Min {get;set;}

        [Column("I3Max")]
        public double I3Max {get;set;}

        [NotMapped]
        public double MinimalInterest{
            get{
                return Math.Round(Math.Min(Math.Min(I1Min, I2Min), I3Min) * MaximalInterest, 2);
            }
        }

        [NotMapped]
        public Tuple<double,double, double>[] InterestProbability{
            get{
                var allConstants = allIntervalConstants.OrderBy(c => c.Item1).ThenBy(c => c.Item2);
                List<Tuple<double, double, double>> res = new List<Tuple<double, double, double>>();
                double val = allConstants.First().Item2;
                double from = allConstants.First().Item1;
                foreach(var act in allConstants)
                {
                    if(act == allConstants.First()) continue;
                    res.Add(new Tuple<double, double, double>(from, act.Item1, val));
                    val += act.Item2;
                    from = act.Item1;
                }
                return res.Where(f => f.Item1 != f.Item2).ToArray();
            }
        }

        [NotMapped]
        private Tuple<double, double>[] allIntervalConstants{
            get
            {
                double[] limits = CyclePartsLimits;
                Tuple<double, double>[] res = {
                    new Tuple<double, double>(Math.Round(I1Min * MaximalInterest, 2), CyclePartsLimits[0]),
                    new Tuple<double, double>(Math.Round(I1Max * MaximalInterest, 2), -CyclePartsLimits[0]),
                    new Tuple<double, double>(Math.Round(I2Min * MaximalInterest, 2),  CyclePartsLimits[1] - CyclePartsLimits[0]),
                    new Tuple<double, double>(Math.Round(I2Max * MaximalInterest, 2),  CyclePartsLimits[0] - CyclePartsLimits[1]),
                    new Tuple<double, double>(Math.Round(I3Min * MaximalInterest, 2), 1 - CyclePartsLimits[1]),
                    new Tuple<double, double>(Math.Round(I3Max * MaximalInterest, 2),  CyclePartsLimits[1] - 1),
                };
                return res;
            }
        }

        [NotMapped]
        public static double[] CyclePartsLimits{
            get{
                double[] res = {0.4, 0.9};
                return res;
            }    
        }

        public double GenerateInterest(DateTime time)
        {
            double position = EconomyUtils.GetActualCyclePosition(time);
            double[] limits = CyclePartsLimits;
            if(position < limits[0])
            {
                return MaximalInterest * BasicUtils.GenRandomDouble(I1Min, I1Max);
            }
            else if(position < limits[1])
            {
                return MaximalInterest * BasicUtils.GenRandomDouble(I2Min, I2Max);
            }
            else
            {
                return MaximalInterest * BasicUtils.GenRandomDouble(I3Min, I3Max);
            }
        }
    }
}