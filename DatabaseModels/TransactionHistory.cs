using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace DatabaseModels
{
    [Table("TransactionHistory")]
    public class TransactionHistory
    {
        [Column("Account")]
        public string AccountNumber {get; set;}

        [Column("OldBalance")]
        public double OldBalance {get;set;}

        [Column("NewBalance")]
        public double NewBalance {get;set;}

        [NotMapped]
        public double Change{
            get
            {
                return Math.Round(NewBalance - OldBalance,2);
            }
        }

        [Column("Time")]
        public DateTime Date {get;set;}

        [Column("Counterfact")]
        public string CounterfactNumber {get; set;}

        [Column("Message")]
        public string Message {get; set;}

        [Column("VariableSymbol")]
        public int VariableSymbol{get;set;}

        [ForeignKey("AccountNumber")]
        public virtual Account Account{get; set;}


    }
}