using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Utils;

namespace DatabaseModels
{
    [Table("WaitingActions")]
    public class ScheduleAction
    {
        public ScheduleAction(){

        }
        public ScheduleAction(ScheduleAction action){
            Id = action.Id;
            Created = action.Created;
            ActionTypeId = action.ActionTypeId;
            ActionDate = action.ActionDate;
            Description = action.Description;
            Data = action.Data;
            DataDescription = action.DataDescription;
            UserId = action.UserId;
            User = action.User;
        }

        [Key]
        [Column("Id")]
        public int Id {get;set;}

        [Column("CreatedDate")]
        public DateTime Created {get;set;}

        [Column("DateOfAction")]
        public DateTime ActionDate {get;set;}

        [Column("ActionTypeId")]
        public int ActionTypeId {get;set;}

        [Column("Description")]
        public string Description {get;set;}

        [Column("Data")]
        public string Data {get;set;}

        [Column("DataDescription")]
        public string DataDescription {get;set;}

        [Column("UserId")]
        public int UserId {get;set;}

        [ForeignKey("UserId")]
        public virtual User User {get;set;}
    }

    public class PaySavingsAccountAction : ScheduleAction
    {
        public PaySavingsAccountAction(){
            Created = DateTime.Now;
            ActionTypeId = ScheduleActionType.PaySavingsAccountAction;
            DataDescription = "Time of creation of Savings account that specifies it in format: yyyyMMddHHmmssfffffff;collect money from account";
            Description = "Pay for savings account";
            Data = ";";
        }

        public PaySavingsAccountAction(ScheduleAction action):base(action){
            
        }

        [NotMapped]
        public DateTime TimeOfSavingsAccountCreation{
            get{
                string[] data = Data.Split(';');
                return DateTime.ParseExact(data[0], "yyyyMMddHHmmssfffffff", null);
            }
            set{
                string dateString = value.ToString("yyyyMMddHHmmssfffffff");
                string[] data = Data.Split(';');
                data[0] = dateString;
                Data = string.Join(";", data);
            }
        }

        [NotMapped]
        public bool CollectMoney{
            get{
                string[] data = Data.Split(';');
                return bool.Parse(data[1]);
            }
            set{
                string[] data = Data.Split(';');
                data[1] = value.ToString();
                Data = string.Join(";", data);
            }
        }
    }

    public class HouseholdExpenditureAction : ScheduleAction
    {
        public HouseholdExpenditureAction(){
            Created = DateTime.Now;
            ActionTypeId = ScheduleActionType.HouseholdExpenditureAction;
            DataDescription = "";
            Data = "";
            Description = "Schedule household expenditure payment";
        }
        public HouseholdExpenditureAction(ScheduleAction action) : base(action){}
    } 

    public class InvestitionFondInterestAction : ScheduleAction
    {
        public InvestitionFondInterestAction()
        {
            Created = DateTime.Now;
            ActionTypeId = ScheduleActionType.InvestitionFondInterest;
            DataDescription = "TimeOfCreationOfInvestitionFond";
            Data = "";
            Description = "Investition fond interest";
        }
        public InvestitionFondInterestAction(ScheduleAction action) : base(action){}
    
        [NotMapped]
        public DateTime TimeOfFondCreation{
            get{
                return DateTime.ParseExact(Data, ApplicationConstants.SAVING_DATETTIME_AS_STRING_FORMAT, null);
            }
            set{
                Data = value.ToString(ApplicationConstants.SAVING_DATETTIME_AS_STRING_FORMAT);
            }
        }
    }

    public class PayInsuranceAction : ScheduleAction
    {
        public PayInsuranceAction()
        {
            Created = DateTime.Now;
            ActionTypeId = ScheduleActionType.PayInsuranceAction;
            DataDescription = "InsuranceType";
            Data = "";
            Description = "Pay for insurace";
        }
        public PayInsuranceAction(ScheduleAction action) : base(action){}

        public Insurance Insurance{
            get{
                int type = int.Parse(Data);
                Insurance insurace = User.Properties.Account.Insurances.Where(i => i.InsuranceType == type).SingleOrDefault();
                if(insurace == null) return null;
                return insurace.SpecificInsurance;
            }
            set{
                Data = value.InsuranceType.ToString();
            }
        }
    }

    public static class ScheduleActionType
    {
        public const int PaySavingsAccountAction = 0;
        public const int InvestitionFondInterest = 1;
        public const int HouseholdExpenditureAction = 2;
        public const int PayInsuranceAction = 3;
    }
}