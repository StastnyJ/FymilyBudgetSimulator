using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using Utils;

namespace DatabaseModels
{
    [Table("SavingsAccounts")]
    public class SavingsAccount
    {        
        [Column("Account")]
        public string AccountId {get; set;}

        [Column("CreationTime")]
        public DateTime CreationTime {get;set;}

        [Column("AccountType")]
        public int AccountTypeId {get;set;}

        [Column("MonthlyFee")]
        public int MonthlyFee {get; set;}

        [Column("Duration")] //months
        public int Duration {get;set;}

        [Column("InvestedMoney")]
        public int InvestedMoney {get;set;}
        
        [ForeignKey("AccountId")]
        public virtual Account Account {get; set;}

        [ForeignKey("AccountTypeId")]
        public virtual SavingsAccountType AccountType {get;set;}
       
        [NotMapped]
        public double Interest
        {
            get
            {
                return SavingsAccount.GetInterest(AccountType, MonthlyFee, Duration);
            }
        }

        [NotMapped]
        public double Subsidy
        {
            get
            {
                return SavingsAccount.GetSubsidy(AccountType, MonthlyFee, Duration);
            }
        }

        [NotMapped]
        public DateTime DueDate
        {
            get
            {
                return UserUtils.CountVirtualDate(Account.UserProperties.User.TimeOfCreation, this.CreationTime, Account.UserProperties.VirtualDayDuration).AddMonths(Duration);
            }
        }

        [NotMapped]
        public int ReamingFees
        {
            get{
                return Duration - (InvestedMoney / MonthlyFee);
            }
        }

        [NotMapped]
        public double MoneyWithInterest
        {
            get{
                return InvestedMoney * Math.Pow(1 + (Interest / 100), (Duration - ReamingFees) / 12);
            }
        }

        public static double GetInterest(SavingsAccountType type, int monthlyFee, int duration)
        {
            return type.C4 / (1 + Math.Pow(Math.E, (type.C0 - (duration * monthlyFee)) / type.C1)) + type.C2 - ((Math.Abs(type.C3 - duration)) / type.C5);
        }

        public static double GetSubsidy(SavingsAccountType type, int monthlyFee, int duration)
        {
            return type.C7 * Math.Floor(1 + Math.Max(1, duration / 12) * (monthlyFee * duration) / type.C6);
        }

    }

    [Table("SavingsAccountTypes")]
    public class SavingsAccountType
    {
        [Key]
        [Column("Id")]
        public int Id {get; set;}

        [Column("Description")]
        public string Description {get;set;}

        [Column("MinimalDuration")]
        public int MinimalDuration{get;set;}

        [Column("MaximalDuration")]
        public int MaximalDuration{get;set;}

        [Column("MinimalFee")]
        public int MinimalFee {get;set;}

        [Column("MaximalFee")]
        public int MaximalFee {get;set;}

        [Column("FeeStep")]
        public int FeeStep{get;set;}

        [Column("WithdrawalFee")]
        public int WithdrawalFee{get;set;}

        [Column("Constant0")]
        public double C0 {get;set;}

        [Column("Constant1")]
        public double C1 {get;set;}

        [Column("Constant2")]
        public double C2 {get;set;}
        
        [Column("Constant3")]
        public double C3 {get;set;}
        
        [Column("Constant4")]
        public double C4 {get;set;}
        
        [Column("Constant5")]
        public double C5 {get;set;}
        
        [Column("Constant6")]
        public double C6 {get;set;}
        
        [Column("Constant7")]
        public double C7 {get;set;}

        public bool CheckValidFeeAndDuration(int fee, int duration)
        {
            return fee >= MinimalFee && fee <= MaximalFee && duration >= MinimalDuration && duration <= MaximalDuration;
        }

        public double GetMaxInterest()
        {
            double max = -1;
            for(int i = MinimalDuration; i <= MaximalDuration; i++)
            {
                double interest = SavingsAccount.GetInterest(this, MaximalFee, i);
                if(interest > max) max = interest;
            }
            return max;
        }

    }
}