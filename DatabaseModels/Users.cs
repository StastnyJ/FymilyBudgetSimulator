﻿using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace DatabaseModels
{
    [Table("Users")]
    public class User
    {  
        
        [Column("UserLogin")]
        public string Login {get; set;}
        
        [Key]      
        [Column("UserId")]
        public int Id {get; set;}

        [Column("PasswordHash")]
        public string PasswordHash {get; set;}
        
        [Column("UserEmail")]
        public string Email {get; set;}

        [Column("CreatedTime")]
        public DateTime TimeOfCreation {get; set;}

        [Column("LastLoggedIn")]
        public DateTime TimeOfLastLogIn {get; set;}

        [Column("RoleId")]
        public int RoleId{get; set;}

        [ForeignKey("RoleId")]        
        public virtual Role Role{get; set;}

        public virtual Career Career{get; set;}

        public virtual UserProperties Properties {get; set;}
    }   
}
