using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace DatabaseModels
{
    [Table("Career")]
    public class Career
    {
        [Key]
        [Column("Id")]
        public int Id {get; set;}

        [Column("UserId")]
        public int UserId {get; set;}

        [Column("CareerTypeId")]
        public int CareerTypeId {get; set;}

        [Column("CreatedTime")]
        public DateTime CreatedTime {get; set;}
        
        [ForeignKey("UserId")]
        public virtual User User {get; set;}

        [ForeignKey("CareerTypeId")]
        public virtual CarrerType CareerType {get; set;}
    }

    [Table("CareerType")]
    public class CarrerType
    {
        [Key]
        [Column("Id")]
        public int Id {get;set;}

        [Column("Description")]
        public string Description {get;set;}

        [Column("CareerFlowId")]
        public int FlowId {get; set;}

        [Column("CareerFlowSteps")]
        public int FlowSteps {get; set;}
    }

    public static class CareerTypes
    {
        public static int Engeneer = 0;
        public static int Teacher = 1;
        public static int Doctor = 2;
        public static int Official = 3;
        public static int Labourer = 4;

        public static int[] All = {Engeneer, Teacher, Doctor, Official, Labourer};
    }
}