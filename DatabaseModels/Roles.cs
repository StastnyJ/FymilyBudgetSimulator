using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace DatabaseModels
{
    [Table("Roles")]
    public class Role
    {
        [Key]
        [Column("Id")]
        public int Id {get; set;}
        
        [Column("Description")]
        public string Description{get; set;}
    }

     public static class Roles
    {
        public const int FreeUser = 0;
        public const int PaiedUser = 1;
        public const int Student = 2;
        public const int Teacher = 3;
        public static readonly int[] All = {FreeUser, PaiedUser, Student, Teacher };
    }
}
