using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace DatabaseModels
{
    [Table("BasicUsersInformation")]
    public class UserProperties
    {   
        [Key]      
        [Column("UserId")]
        public int UserId {get; set;}

        [ForeignKey("UserId")]
        public virtual User User {get;set;}

        [Column("JobId")]
        public int JobId {get; set;}   

        [Column("Happiness")]
        public int Happiness {get; set;}

        [Column("FirstCarId")]
        public int FirstCarId {get;set;}

        [Column("SecondCarId")]
        public int SecondCarId {get;set;}

        [Column("EquipmentId")]
        public int EquipmentId {get;set;}

        [Column("HouseId")]
        public int HouseId {get;set;}
        
        [Column("HouseholdExpenditure")]
        public int HouseholdExpenditure {get;set;}

        [Column("Account")]
        public string AccountNumber {get;set;}

        [Column("VirtualDayDuration")]
        public double VirtualDayDuration {get;set;}

        [ForeignKey("AccountNumber")]
        public virtual Account Account {get; set;}
    }   
}
