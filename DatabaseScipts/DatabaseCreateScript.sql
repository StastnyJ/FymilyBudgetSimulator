-- ****************** SqlDBM: MySQL ******************;
-- ***************************************************;

DROP TABLE CarsClass;


DROP TABLE House;


DROP TABLE Equipment;


DROP TABLE Cars;


DROP TABLE CareerFlow;


DROP TABLE Users;


DROP TABLE SavingsAccounts;


DROP TABLE Fonds;


DROP TABLE Insurances;


DROP TABLE Loans;


DROP TABLE TransactionHistory;


DROP TABLE BasicUsersInformation;


DROP TABLE EventsHistory;


DROP TABLE CareerType;


DROP TABLE SavingsAccountTypes;


DROP TABLE FondTypes;


DROP TABLE Accounts;


DROP TABLE Jobs;


DROP TABLE HouseStates;


DROP TABLE HappinessHistory;


DROP TABLE Career;


DROP TABLE Roles;


DROP TABLE WaitingActions;



-- ************************************** CommingEvents

CREATE TABLE WaitingActions
(
 Id              INT NOT NULL,
 CreatedDate      DATETIME NOT NULL,
 DateOfAction    DATETIME NOT NULL,
 ActionTypeId    INT NOT NULL,
 Description     VARCHAR(1000) NOT NULL,
 Data            VARCHAR(10000) NOT NULL,
 DataDescription VARCHAR(10000) NOT NULL,
 UserId          INT,

PRIMARY KEY (Id)
);






-- ************************************** SavingsAccountTypes

CREATE TABLE SavingsAccountTypes
(
 Id INT NOT NULL,
 Description VARCHAR(1000) NOT NULL,
 MinimalDuration INT NOT NULL,
 MaximalDuration INT NOT NULL,
 MinimalFee      INT NOT NULL,
 MaximalFee       INT NOT NULL,
 FeeStep         INT NOT NULL,
 WithdrawalFee   INT NOT NULL,
 Constant0       FLOAT NOT NULL,
 Constant1       FLOAT NOT NULL,
 Constant2       FLOAT NOT NULL,
 Constant3       FLOAT NOT NULL,
 Constant4       FLOAT NOT NULL,
 Constant5       FLOAT NOT NULL,
 Constant6       FLOAT NOT NULL,
 Constant7       FLOAT NOT NULL,
 
 PRIMARY KEY (Id)
);





-- ************************************** FondTypes


CREATE TABLE FondTypes
(
 Id              INT NOT NULL,
 Description     VARCHAR(1000) NOT NULL,
 MaximalInterest FLOAT NOT NULL,
 I1Min           FLOAT NOT NULL,
 I1Max           FLOAT NOT NULL,
 I2Min           FLOAT NOT NULL,
 I2Max           FLOAT NOT NULL,
 I3Min           FLOAT NOT NULL,
 I3Max           FLOAT NOT NULL,

PRIMARY KEY (Id)
);



-- ************************************** Accounts

CREATE TABLE Accounts
(
 Account VARCHAR(20) NOT NULL,
 Balance FLOAT NOT NULL,

PRIMARY KEY (Account)
);





-- ************************************** Jobs

CREATE TABLE Jobs
(
 JobId      INT NOT NULL,
 UserId     INT NOT NULL,
 Salay      INT NOT NULL,
 SalaryDate INT NOT NULL,
 LastSalary DATETIME NOT NULL,
 Holidays   INT NOT NULL,
 Position   INT NOT NULL,

PRIMARY KEY (JobId)
);





-- ************************************** HouseStates

CREATE TABLE HouseStates
(
 State INT NOT NULL,
 Description VARCHAR(1000) NOT NULL,

PRIMARY KEY (State)
);





-- ************************************** HappinessHistory

CREATE TABLE HappinessHistory
(
 Id             INT NOT NULL,
 UserId         INT NOT NULL,
 OldValue       INT NOT NULL,
 NewValue       INT NOT NULL,
 Time           DATETIME NOT NULL,
 Reason         VARCHAR(1000) NOT NULL,
 EventReference INT,

PRIMARY KEY (Id)
);





-- ************************************** Career

CREATE TABLE Career
(
 Id           INT NOT NULL,
 UserId       INT NOT NULL,
 CareerTypeId INT NOT NULL,
 CreatedTime  DATETIME NOT NULL,

PRIMARY KEY (Id)
);





-- ************************************** SavingsAccounts

CREATE TABLE SavingsAccounts
(
 Account       VARCHAR(20) NOT NULL,
 CreationTime  DATETIME NOT NULL,
 AccountType   INT NOT NULL,
 MonthlyFee    INT NOT NULL,
 Duration      INT NOT NULL,
 InvestedMoney INT NOT NULL,

PRIMARY KEY (Account, CreationTime)
);





-- ************************************** Fonds

CREATE TABLE Fonds
(
 CreationTime  DATETIME NOT NULL,
 AccountNumber DATETIME NOT NULL,
 FondType      INT NOT NULL,
 InvestedMoney FLOAT NOT NULL,
 ActualMoney   FLOAT NOT NULL,

PRIMARY KEY (CreationTime, AccountNumber)
);





-- ************************************** Insurances

CREATE TABLE Insurances
(
 CreationTime    DATETIME NOT NULL,
 Account         VARCHAR(20) NOT NULL,
 Type            INT NOT NULL,
 InsuredMoney    FLOAT NOT NULL,
 Data            VARCHAR(1000) NOT NULL,
 DataDescription VARCHAR(1000) NOT NULL,

PRIMARY KEY (Type, Account)
);





-- ************************************** Loans

CREATE TABLE Loans
(
 Id                 INT NOT NULL,
 Account            VARCHAR(20) NOT NULL,
 Ammount            INT NOT NULL,
 Time               DATETIME NOT NULL,
 MonthlyInstallment INT NOT NULL,

PRIMARY KEY (Id)
);





-- ************************************** TransactionHistory

CREATE TABLE TransactionHistory
(
 Account        VARCHAR(20) NOT NULL,
 OldBalance     FLOAT NOT NULL,
 NewBalance     FLOAT NOT NULL,
 Time           DATETIME NOT NULL,
 Counterfact    VARCHAR(20) NOT NULL,
 Message        VARCHAR(1000),
 VariableSymbol INT,

PRIMARY KEY (Account, Time)
);





-- ************************************** BasicUsersInformation

CREATE TABLE BasicUsersInformation
(
 UserId               INT NOT NULL,
 Happiness            INT NOT NULL,
 FirstCarId           INT NOT NULL,
 SecondCarId          INT NOT NULL,
 EquipmentId          INT NOT NULL,
 HouseId              INT NOT NULL,
 HouseholdExpenditure INT NOT NULL,
 VirtualDayDuration   FLOAT NOT NULL, 
 JobId                INT NOT NULL,
 Account              VARCHAR(20) NOT NULL,

PRIMARY KEY (UserId)
);





-- ************************************** EventsHistory

CREATE TABLE EventsHistory
(
 Id          INT NOT NULL,
 Name        VARCHAR(1000) NOT NULL,
 Description VARCHAR(1000) NOT NULL,
 Result      VARCHAR(1000) NOT NULL,
 Time        DATETIME NOT NULL,
 Insured     INT NOT NULL,
 UserId      INT NOT NULL,

PRIMARY KEY (Id)
);





-- ************************************** CareerType

CREATE TABLE CareerType
(
 Id              INT NOT NULL,
 Description     VARCHAR(45) NOT NULL,
 CareerFlowId    INT NOT NULL,
 CareerFlowSteps INT NOT NULL,

PRIMARY KEY (Id)
);





-- ************************************** House

CREATE TABLE House
(
 Id    INT NOT NULL,
 Value INT NOT NULL,
 State INT NOT NULL,

PRIMARY KEY (Id)
);





-- ************************************** Equipment

CREATE TABLE Equipment
(
 Id         INT NOT NULL,
 Value      INT NOT NULL,
 AverageAge INT NOT NULL,
 Ammount    INT NOT NULL,

PRIMARY KEY (Id)
);





-- ************************************** Cars

CREATE TABLE Cars
(
 Id             INT NOT NULL,
 Class          INT NOT NULL,
 TimeOfCreation DATETIME NOT NULL,
 State          INT NOT NULL,
 Distance       INT NOT NULL,

PRIMARY KEY (Id)
);





-- ************************************** CareerFlow

CREATE TABLE CareerFlow
(
 FlowId       INT NOT NULL,
 FlowStep     INT NOT NULL,
 Description  VARCHAR(45) NOT NULL,
 PaymentIndex INT NOT NULL,

PRIMARY KEY (FlowId, FlowStep)
);





-- ************************************** Users

CREATE TABLE Users
(
 UserLogin    VARCHAR(32) NOT NULL,
 UserId       INT NOT NULL,
 PasswordHash VARCHAR(64) NOT NULL,
 RoleId       INT,
 UserEmail    VARCHAR(64) NOT NULL,
 CreatedTime  DATETIME NOT NULL,
 LastLoggedIn DATETIME NOT NULL,

PRIMARY KEY (UserLogin)
);






-- ************************************** CarsClass

CREATE TABLE CarsClass
(
 ClassId         INT NOT NULL,
 Description     VARCHAR(1000) NOT NULL,
 PriceIndex      INT NOT NULL,
 ExperitureIndex INT NOT NULL,
 HappinessIndex  INT NOT NULL,

PRIMARY KEY (ClassId)
);



-- ************************************** CommingEvents

CREATE TABLE Roles
(   
 Id INT NOT NULL,
 Description VARCHAR(1000) NOT NULL,

PRIMARY KEY (Id)
);
