-----------------------ROLES-----------------------------

INSERT INTO Roles(Id, Description) values(0, "FreeUser");
INSERT INTO Roles(Id, Description) values(1, "PaiedUser");
INSERT INTO Roles(Id, Description) values(2, "Student");
INSERT INTO Roles(Id, Description) values(3, "Teacher");


-----------------------CAREER TYPES-----------------------

INSERT INTO CareerType(Id, Description, CareerFlowId, CareerFlowSteps) values(0, "Engeener", 0, 5);
INSERT INTO CareerType(Id, Description, CareerFlowId, CareerFlowSteps) values(1, "Teacher", 1, 5);
INSERT INTO CareerType(Id, Description, CareerFlowId, CareerFlowSteps) values(2, "Doctor", 2, 5);
INSERT INTO CareerType(Id, Description, CareerFlowId, CareerFlowSteps) values(3, "Official", 3, 5);
INSERT INTO CareerType(Id, Description, CareerFlowId, CareerFlowSteps) values(4, "Labourer", 4, 5);


-----------------------SAVINGS ACCOUNTS TYPES-------------

INSERT INTO SavingsAccountTypes(Id, Description, MinimalDuration, MaximalDuration, MinimalFee, MaximalFee, FeeStep, WithdrawalFee, Constant0, Constant1, Constant2, Constant3, Constant4, Constant5, Constant6, Constant7) values (0, "Building Saving", 72, 600, 10, 500, 1, 500, 570, 600, 0.23, 112, 2, 1400, 6900, 30);


-----------------------INVESTITION FONDS TYPES------------

INSERT INTO FondTypes(Id, Description, MaximalInterest, I1Min, I1Max, I2Min, I2Max, I3Min, I3Max) VALUES (0, "Bond fond", 0.02, 0.7, 1, 0.5, 0.75, 0, 0.5);
INSERT INTO FondTypes(Id, Description, MaximalInterest, I1Min, I1Max, I2Min, I2Max, I3Min, I3Max) VALUES (1, "Action fond", 0.05, 0, 1, -0.2, 0.6, -0.5, 0.2);
INSERT INTO FondTypes(Id, Description, MaximalInterest, I1Min, I1Max, I2Min, I2Max, I3Min, I3Max) VALUES (2, "Mixed fond", 0.03, 0.2, 1, -0.1, 0.7, -0.3, 0.4);